package ru.karpov.tm;

import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.karpov.tm.command.AbstractCommand;
import ru.karpov.tm.context.Bootstrap;

import java.util.Set;

public final class Application {
    public static void main(@NotNull final String[] args) throws Exception {
        @NotNull final Set<Class<? extends AbstractCommand>> commandClasses =
                new Reflections("ru.karpov.tm.command").getSubTypesOf(AbstractCommand.class);
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(commandClasses);
    }
}
