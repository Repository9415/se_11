
package ru.karpov.tm.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.karpov.tm.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AbstractException_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "AbstractException");
    private final static QName _Exception_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "Exception");
    private final static QName _DataBinaryLoadBySerialization_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataBinaryLoadBySerialization");
    private final static QName _DataBinaryLoadBySerializationResponse_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataBinaryLoadBySerializationResponse");
    private final static QName _DataBinarySaveBySerialization_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataBinarySaveBySerialization");
    private final static QName _DataBinarySaveBySerializationResponse_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataBinarySaveBySerializationResponse");
    private final static QName _DataJsonLoadByFasterXml_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataJsonLoadByFasterXml");
    private final static QName _DataJsonLoadByFasterXmlResponse_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataJsonLoadByFasterXmlResponse");
    private final static QName _DataJsonLoadByJaxB_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataJsonLoadByJaxB");
    private final static QName _DataJsonLoadByJaxBResponse_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataJsonLoadByJaxBResponse");
    private final static QName _DataJsonSaveByFasterXml_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataJsonSaveByFasterXml");
    private final static QName _DataJsonSaveByFasterXmlResponse_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataJsonSaveByFasterXmlResponse");
    private final static QName _DataJsonSaveByJaxB_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataJsonSaveByJaxB");
    private final static QName _DataJsonSaveByJaxBResponse_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataJsonSaveByJaxBResponse");
    private final static QName _DataXmlLoadByFasterXml_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataXmlLoadByFasterXml");
    private final static QName _DataXmlLoadByFasterXmlResponse_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataXmlLoadByFasterXmlResponse");
    private final static QName _DataXmlLoadByJaxB_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataXmlLoadByJaxB");
    private final static QName _DataXmlLoadByJaxBResponse_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataXmlLoadByJaxBResponse");
    private final static QName _DataXmlSaveByFasterXml_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataXmlSaveByFasterXml");
    private final static QName _DataXmlSaveByFasterXmlResponse_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataXmlSaveByFasterXmlResponse");
    private final static QName _DataXmlSaveByJaxB_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataXmlSaveByJaxB");
    private final static QName _DataXmlSaveByJaxBResponse_QNAME = new QName("http://endpoint.api.tm.karpov.ru/", "dataXmlSaveByJaxBResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.karpov.tm.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AbstractException }
     * 
     */
    public AbstractException createAbstractException() {
        return new AbstractException();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link DataBinaryLoadBySerialization }
     * 
     */
    public DataBinaryLoadBySerialization createDataBinaryLoadBySerialization() {
        return new DataBinaryLoadBySerialization();
    }

    /**
     * Create an instance of {@link DataBinaryLoadBySerializationResponse }
     * 
     */
    public DataBinaryLoadBySerializationResponse createDataBinaryLoadBySerializationResponse() {
        return new DataBinaryLoadBySerializationResponse();
    }

    /**
     * Create an instance of {@link DataBinarySaveBySerialization }
     * 
     */
    public DataBinarySaveBySerialization createDataBinarySaveBySerialization() {
        return new DataBinarySaveBySerialization();
    }

    /**
     * Create an instance of {@link DataBinarySaveBySerializationResponse }
     * 
     */
    public DataBinarySaveBySerializationResponse createDataBinarySaveBySerializationResponse() {
        return new DataBinarySaveBySerializationResponse();
    }

    /**
     * Create an instance of {@link DataJsonLoadByFasterXml }
     * 
     */
    public DataJsonLoadByFasterXml createDataJsonLoadByFasterXml() {
        return new DataJsonLoadByFasterXml();
    }

    /**
     * Create an instance of {@link DataJsonLoadByFasterXmlResponse }
     * 
     */
    public DataJsonLoadByFasterXmlResponse createDataJsonLoadByFasterXmlResponse() {
        return new DataJsonLoadByFasterXmlResponse();
    }

    /**
     * Create an instance of {@link DataJsonLoadByJaxB }
     * 
     */
    public DataJsonLoadByJaxB createDataJsonLoadByJaxB() {
        return new DataJsonLoadByJaxB();
    }

    /**
     * Create an instance of {@link DataJsonLoadByJaxBResponse }
     * 
     */
    public DataJsonLoadByJaxBResponse createDataJsonLoadByJaxBResponse() {
        return new DataJsonLoadByJaxBResponse();
    }

    /**
     * Create an instance of {@link DataJsonSaveByFasterXml }
     * 
     */
    public DataJsonSaveByFasterXml createDataJsonSaveByFasterXml() {
        return new DataJsonSaveByFasterXml();
    }

    /**
     * Create an instance of {@link DataJsonSaveByFasterXmlResponse }
     * 
     */
    public DataJsonSaveByFasterXmlResponse createDataJsonSaveByFasterXmlResponse() {
        return new DataJsonSaveByFasterXmlResponse();
    }

    /**
     * Create an instance of {@link DataJsonSaveByJaxB }
     * 
     */
    public DataJsonSaveByJaxB createDataJsonSaveByJaxB() {
        return new DataJsonSaveByJaxB();
    }

    /**
     * Create an instance of {@link DataJsonSaveByJaxBResponse }
     * 
     */
    public DataJsonSaveByJaxBResponse createDataJsonSaveByJaxBResponse() {
        return new DataJsonSaveByJaxBResponse();
    }

    /**
     * Create an instance of {@link DataXmlLoadByFasterXml }
     * 
     */
    public DataXmlLoadByFasterXml createDataXmlLoadByFasterXml() {
        return new DataXmlLoadByFasterXml();
    }

    /**
     * Create an instance of {@link DataXmlLoadByFasterXmlResponse }
     * 
     */
    public DataXmlLoadByFasterXmlResponse createDataXmlLoadByFasterXmlResponse() {
        return new DataXmlLoadByFasterXmlResponse();
    }

    /**
     * Create an instance of {@link DataXmlLoadByJaxB }
     * 
     */
    public DataXmlLoadByJaxB createDataXmlLoadByJaxB() {
        return new DataXmlLoadByJaxB();
    }

    /**
     * Create an instance of {@link DataXmlLoadByJaxBResponse }
     * 
     */
    public DataXmlLoadByJaxBResponse createDataXmlLoadByJaxBResponse() {
        return new DataXmlLoadByJaxBResponse();
    }

    /**
     * Create an instance of {@link DataXmlSaveByFasterXml }
     * 
     */
    public DataXmlSaveByFasterXml createDataXmlSaveByFasterXml() {
        return new DataXmlSaveByFasterXml();
    }

    /**
     * Create an instance of {@link DataXmlSaveByFasterXmlResponse }
     * 
     */
    public DataXmlSaveByFasterXmlResponse createDataXmlSaveByFasterXmlResponse() {
        return new DataXmlSaveByFasterXmlResponse();
    }

    /**
     * Create an instance of {@link DataXmlSaveByJaxB }
     * 
     */
    public DataXmlSaveByJaxB createDataXmlSaveByJaxB() {
        return new DataXmlSaveByJaxB();
    }

    /**
     * Create an instance of {@link DataXmlSaveByJaxBResponse }
     * 
     */
    public DataXmlSaveByJaxBResponse createDataXmlSaveByJaxBResponse() {
        return new DataXmlSaveByJaxBResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AbstractException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "AbstractException")
    public JAXBElement<AbstractException> createAbstractException(AbstractException value) {
        return new JAXBElement<AbstractException>(_AbstractException_QNAME, AbstractException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinaryLoadBySerialization }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataBinaryLoadBySerialization")
    public JAXBElement<DataBinaryLoadBySerialization> createDataBinaryLoadBySerialization(DataBinaryLoadBySerialization value) {
        return new JAXBElement<DataBinaryLoadBySerialization>(_DataBinaryLoadBySerialization_QNAME, DataBinaryLoadBySerialization.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinaryLoadBySerializationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataBinaryLoadBySerializationResponse")
    public JAXBElement<DataBinaryLoadBySerializationResponse> createDataBinaryLoadBySerializationResponse(DataBinaryLoadBySerializationResponse value) {
        return new JAXBElement<DataBinaryLoadBySerializationResponse>(_DataBinaryLoadBySerializationResponse_QNAME, DataBinaryLoadBySerializationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinarySaveBySerialization }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataBinarySaveBySerialization")
    public JAXBElement<DataBinarySaveBySerialization> createDataBinarySaveBySerialization(DataBinarySaveBySerialization value) {
        return new JAXBElement<DataBinarySaveBySerialization>(_DataBinarySaveBySerialization_QNAME, DataBinarySaveBySerialization.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinarySaveBySerializationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataBinarySaveBySerializationResponse")
    public JAXBElement<DataBinarySaveBySerializationResponse> createDataBinarySaveBySerializationResponse(DataBinarySaveBySerializationResponse value) {
        return new JAXBElement<DataBinarySaveBySerializationResponse>(_DataBinarySaveBySerializationResponse_QNAME, DataBinarySaveBySerializationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonLoadByFasterXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataJsonLoadByFasterXml")
    public JAXBElement<DataJsonLoadByFasterXml> createDataJsonLoadByFasterXml(DataJsonLoadByFasterXml value) {
        return new JAXBElement<DataJsonLoadByFasterXml>(_DataJsonLoadByFasterXml_QNAME, DataJsonLoadByFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonLoadByFasterXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataJsonLoadByFasterXmlResponse")
    public JAXBElement<DataJsonLoadByFasterXmlResponse> createDataJsonLoadByFasterXmlResponse(DataJsonLoadByFasterXmlResponse value) {
        return new JAXBElement<DataJsonLoadByFasterXmlResponse>(_DataJsonLoadByFasterXmlResponse_QNAME, DataJsonLoadByFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonLoadByJaxB }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataJsonLoadByJaxB")
    public JAXBElement<DataJsonLoadByJaxB> createDataJsonLoadByJaxB(DataJsonLoadByJaxB value) {
        return new JAXBElement<DataJsonLoadByJaxB>(_DataJsonLoadByJaxB_QNAME, DataJsonLoadByJaxB.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonLoadByJaxBResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataJsonLoadByJaxBResponse")
    public JAXBElement<DataJsonLoadByJaxBResponse> createDataJsonLoadByJaxBResponse(DataJsonLoadByJaxBResponse value) {
        return new JAXBElement<DataJsonLoadByJaxBResponse>(_DataJsonLoadByJaxBResponse_QNAME, DataJsonLoadByJaxBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonSaveByFasterXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataJsonSaveByFasterXml")
    public JAXBElement<DataJsonSaveByFasterXml> createDataJsonSaveByFasterXml(DataJsonSaveByFasterXml value) {
        return new JAXBElement<DataJsonSaveByFasterXml>(_DataJsonSaveByFasterXml_QNAME, DataJsonSaveByFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonSaveByFasterXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataJsonSaveByFasterXmlResponse")
    public JAXBElement<DataJsonSaveByFasterXmlResponse> createDataJsonSaveByFasterXmlResponse(DataJsonSaveByFasterXmlResponse value) {
        return new JAXBElement<DataJsonSaveByFasterXmlResponse>(_DataJsonSaveByFasterXmlResponse_QNAME, DataJsonSaveByFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonSaveByJaxB }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataJsonSaveByJaxB")
    public JAXBElement<DataJsonSaveByJaxB> createDataJsonSaveByJaxB(DataJsonSaveByJaxB value) {
        return new JAXBElement<DataJsonSaveByJaxB>(_DataJsonSaveByJaxB_QNAME, DataJsonSaveByJaxB.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonSaveByJaxBResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataJsonSaveByJaxBResponse")
    public JAXBElement<DataJsonSaveByJaxBResponse> createDataJsonSaveByJaxBResponse(DataJsonSaveByJaxBResponse value) {
        return new JAXBElement<DataJsonSaveByJaxBResponse>(_DataJsonSaveByJaxBResponse_QNAME, DataJsonSaveByJaxBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlLoadByFasterXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataXmlLoadByFasterXml")
    public JAXBElement<DataXmlLoadByFasterXml> createDataXmlLoadByFasterXml(DataXmlLoadByFasterXml value) {
        return new JAXBElement<DataXmlLoadByFasterXml>(_DataXmlLoadByFasterXml_QNAME, DataXmlLoadByFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlLoadByFasterXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataXmlLoadByFasterXmlResponse")
    public JAXBElement<DataXmlLoadByFasterXmlResponse> createDataXmlLoadByFasterXmlResponse(DataXmlLoadByFasterXmlResponse value) {
        return new JAXBElement<DataXmlLoadByFasterXmlResponse>(_DataXmlLoadByFasterXmlResponse_QNAME, DataXmlLoadByFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlLoadByJaxB }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataXmlLoadByJaxB")
    public JAXBElement<DataXmlLoadByJaxB> createDataXmlLoadByJaxB(DataXmlLoadByJaxB value) {
        return new JAXBElement<DataXmlLoadByJaxB>(_DataXmlLoadByJaxB_QNAME, DataXmlLoadByJaxB.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlLoadByJaxBResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataXmlLoadByJaxBResponse")
    public JAXBElement<DataXmlLoadByJaxBResponse> createDataXmlLoadByJaxBResponse(DataXmlLoadByJaxBResponse value) {
        return new JAXBElement<DataXmlLoadByJaxBResponse>(_DataXmlLoadByJaxBResponse_QNAME, DataXmlLoadByJaxBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlSaveByFasterXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataXmlSaveByFasterXml")
    public JAXBElement<DataXmlSaveByFasterXml> createDataXmlSaveByFasterXml(DataXmlSaveByFasterXml value) {
        return new JAXBElement<DataXmlSaveByFasterXml>(_DataXmlSaveByFasterXml_QNAME, DataXmlSaveByFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlSaveByFasterXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataXmlSaveByFasterXmlResponse")
    public JAXBElement<DataXmlSaveByFasterXmlResponse> createDataXmlSaveByFasterXmlResponse(DataXmlSaveByFasterXmlResponse value) {
        return new JAXBElement<DataXmlSaveByFasterXmlResponse>(_DataXmlSaveByFasterXmlResponse_QNAME, DataXmlSaveByFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlSaveByJaxB }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataXmlSaveByJaxB")
    public JAXBElement<DataXmlSaveByJaxB> createDataXmlSaveByJaxB(DataXmlSaveByJaxB value) {
        return new JAXBElement<DataXmlSaveByJaxB>(_DataXmlSaveByJaxB_QNAME, DataXmlSaveByJaxB.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlSaveByJaxBResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.karpov.ru/", name = "dataXmlSaveByJaxBResponse")
    public JAXBElement<DataXmlSaveByJaxBResponse> createDataXmlSaveByJaxBResponse(DataXmlSaveByJaxBResponse value) {
        return new JAXBElement<DataXmlSaveByJaxBResponse>(_DataXmlSaveByJaxBResponse_QNAME, DataXmlSaveByJaxBResponse.class, null, value);
    }

}
