package ru.karpov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karpov.tm.api.endpoint.*;
import ru.karpov.tm.command.AbstractCommand;
import ru.karpov.tm.service.TerminalService;

import java.util.List;

public interface ServiceLocator {
    @NotNull ITaskEndpoint getTaskEndpoint();

    @NotNull IProjectEndpoint getProjectEndpoint();

    @NotNull IUserEndpoint getUserEndpoint();

    @NotNull ISessionEndpoint getSessionEndpoint();

    @NotNull IDomainEndpoint getDomainEndpoint();

    @NotNull Session getSession();

    void setSession(@Nullable final Session session);

    @Nullable User getCurrentUser();

    void setCurrentUser(@Nullable final User user);

    @NotNull TerminalService getTerminalService();

    @NotNull List<@NotNull AbstractCommand> getCommands();

    @NotNull List<@NotNull String> getListStringCommands();
}
