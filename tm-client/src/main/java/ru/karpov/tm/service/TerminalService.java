package ru.karpov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.service.ITerminalService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@NoArgsConstructor
public class TerminalService implements ITerminalService {

    @NotNull final BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @NotNull
    public String input() throws IOException {
        return input.readLine();
    }

    public void print(@NotNull String string) {
        if (string == null) return;
        System.out.println(string);
    }
}
