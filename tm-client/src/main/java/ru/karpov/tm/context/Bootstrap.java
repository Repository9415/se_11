package ru.karpov.tm.context;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karpov.tm.api.ServiceLocator;
import ru.karpov.tm.api.endpoint.*;
import ru.karpov.tm.command.AbstractCommand;
import ru.karpov.tm.endpoint.*;
import ru.karpov.tm.service.TerminalService;

import java.lang.Exception;
import java.util.*;

public final class Bootstrap implements ServiceLocator {

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @Getter
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpointService().getDomainEndpointPort();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Setter
    @Getter
    @Nullable
    private User currentUser = null;

    @Setter
    @Getter
    @Nullable
    private Session session = null;

    @Getter
    @NotNull
    private TerminalService terminalService = new TerminalService();

    public void initCommands(@NotNull final Set<Class<? extends AbstractCommand>> commandClasses) throws IllegalAccessException, InstantiationException {
        if (commandClasses == null) {
            terminalService.print("Commands not downloaded. The execution program is stopped.");
            System.exit(0);
            return;
        }
        for (@NotNull final Class clazz : commandClasses) {
            @NotNull final AbstractCommand command = (AbstractCommand) clazz.newInstance();
            if (command instanceof AbstractCommand || command != null) {
                command.setServiceLocator(this);
                commands.put(command.getCommandName(), command);
            }
        }
    }

    public void init(@NotNull final Set<Class<? extends AbstractCommand>> commandClasses) throws InstantiationException, IllegalAccessException {
        initCommands(commandClasses);
        terminalService.print("*** Welcome to task manager! ***");
        while (true) {
            try {
                terminalService.print("\nEnter command or \"help\":");
                @NotNull final String inputCommand = terminalService.input().toLowerCase().trim();
                if (inputCommand == null || inputCommand.isEmpty() || !checkCorrectCommand(inputCommand)) {
                    terminalService.print("You entered the wrong command or such a command does not exist. Try again.");
                    continue;
                }

                @NotNull final AbstractCommand command = commands.get(inputCommand);
                if (!command.isAllowed() && currentUser == null) {
                    terminalService.print("To execute a command, you need to log in.");
                    continue;
                }
                command.execute();

            } catch (Exception ex) {
                terminalService.print(ex.getMessage());
            }
        }
    }

    @NotNull
    @Override
    public List<@NotNull AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @NotNull
    @Override
    public List<@NotNull String> getListStringCommands() {
        return new ArrayList<>(commands.keySet());
    }

    public boolean checkCorrectCommand(@NotNull final String inputCommand) {
        return commands.containsKey(inputCommand.toLowerCase());
    }
}