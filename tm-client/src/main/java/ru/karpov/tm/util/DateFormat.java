package ru.karpov.tm.util;

import org.jetbrains.annotations.NotNull;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateFormat {

    @NotNull
    private final static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    @NotNull
    public static Date parseStringToDate(@NotNull final String stringDate) {
        return dateFormatter.parse(stringDate, new ParsePosition(0));
    }

    @NotNull
    public static String parseDateToString(@NotNull final Date date) {
        return dateFormatter.format(date);
    }

    @NotNull
    public static Date parseXMLGregorianToDate(@NotNull final XMLGregorianCalendar date) {
        @NotNull final String stringDate = dateFormatter.format(date.toGregorianCalendar().getTime());
        return parseStringToDate(stringDate);
    }

    @NotNull
    public static Date parseXMLGregorianToDate_2(@NotNull final XMLGregorianCalendar date) {
        @NotNull final Date dt = date.toGregorianCalendar().getTime();
        return dt;
    }

    @NotNull
    public static Date parseDateToXMLGregorian(@NotNull final XMLGregorianCalendar date) {
        @NotNull final String stringDate = dateFormatter.format(date.toGregorianCalendar().getTime());
        return parseStringToDate(stringDate);
    }

    @NotNull
    public static XMLGregorianCalendar parseStringToXMLGregorian(@NotNull final String stringDate) throws DatatypeConfigurationException {
        @NotNull final GregorianCalendar gcalendar = new GregorianCalendar();
        gcalendar.setTime(parseStringToDate(stringDate));
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(gcalendar);
    }
}