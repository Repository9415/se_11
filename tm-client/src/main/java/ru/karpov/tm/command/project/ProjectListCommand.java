package ru.karpov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.*;
import ru.karpov.tm.command.AbstractCommand;

import javax.xml.datatype.XMLGregorianCalendar;
import java.lang.Exception;
import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

@NoArgsConstructor
public final class ProjectListCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }



    @Override
    public void execute() throws Exception, AbstractException_Exception {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final String userId = session.getUserId();
        @NotNull final Collection<Project> projectsRep = projectEndpoint.findAllProjects(session, userId);
        if (projectsRep.isEmpty()) {
            serviceLocator.getTerminalService().print("Projects do not exist.");
            return;
        }

        serviceLocator.getTerminalService().print("1 - in the order of addition;");
        serviceLocator.getTerminalService().print("2 - by start date;");
        serviceLocator.getTerminalService().print("3 - by end date;");
        serviceLocator.getTerminalService().print("4 - by status.");
        serviceLocator.getTerminalService().print("Enter a number to select the sort order:");
        @NotNull final String numberSort = serviceLocator.getTerminalService().input();
        switch (numberSort) {
            case "":
            case "1":
                sortOrderByAdded(projectsRep);
                break;
            case "2":
                sortOrderByStartDate(projectsRep);
                break;
            case "3":
                sortOrderByFinishDate(projectsRep);
                break;
            case "4":
                sortOrderByStatus(projectsRep);
                break;
            default:
                serviceLocator.getTerminalService().print("Incorrect number entered. Try again!");
        }

        serviceLocator.getTerminalService().print("\nDo you want to show a list of tasks attached to project?\n Yes/No");
        @NotNull final String isShowListTasks = serviceLocator.getTerminalService().input().toLowerCase();
        if (isShowListTasks.equalsIgnoreCase("no") || isShowListTasks.isEmpty()) {
            return;
        }
        if (isShowListTasks.equalsIgnoreCase("yes")) {
            serviceLocator.getTerminalService().print("Enter name project:");
            @NotNull final String nameProject = serviceLocator.getTerminalService().input().toLowerCase();
            if (nameProject == null || nameProject.isEmpty()) {
                serviceLocator.getTerminalService().print("Project name entered incorrectly. Try again!");
                return;
            }
            @NotNull String projectId = "";
            for (@NotNull final Project project : projectsRep) {
                if (project.getName().equals(nameProject)) {
                    projectId = project.getId();
                    break;
                }
            }
            showAllTasksByProjectId(projectId);
        }
    }

    void sortOrderByAdded(@NotNull final Collection<Project> projectRep) {
        serviceLocator.getTerminalService().print("List of projects:");
        printProjects(projectRep);
    }

    void sortOrderByStartDate(@NotNull final Collection<Project> projectRep) {
        serviceLocator.getTerminalService().print("List of projects:");
        @NotNull final Collection<Project> projects = projectRep.stream()
                .filter(t -> t.getDateStart() != null)
                .sorted(Comparator.comparing(Project::getDateStart, XMLGregorianCalendar::compare))
                .collect(Collectors.toList());
        printProjects(projects);
    }

    void sortOrderByFinishDate(@NotNull final Collection<Project> projectsRep) {
        serviceLocator.getTerminalService().print("List of projects:");
        @NotNull final Collection<Project> projects = projectsRep.stream()
                .filter(t -> t.getDateFinish() != null)
                .sorted(Comparator.comparing(Project::getDateFinish, XMLGregorianCalendar::compare).reversed())
                .collect(Collectors.toList());
        printProjects(projects);
    }

    void sortOrderByStatus(@NotNull final Collection<Project> projectsRep) {
        serviceLocator.getTerminalService().print("List of projects:");
        @NotNull final Collection<Project> projects = projectsRep.stream()
                .filter(t -> t.getStatus() != null)
                .sorted(Comparator.comparing(Project::getStatus))
                .collect(Collectors.toList());
        printProjects(projects);
    }

    void printProjects(@NotNull final Collection<Project> projectsRep) {
        int j = 1;
        for (@NotNull final Project project : projectsRep) {
            serviceLocator.getTerminalService().print(j + ". " + project.getName());
            j++;
        }
    }

    private void showAllTasksByProjectId(@NotNull final String projectId) throws Exception, AbstractException_Exception {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final String userId = session.getUserId();
        @NotNull final Collection<Task> tasksRep = serviceLocator.getTaskEndpoint().findAllUserTasks(session, userId);
        int j = 1;
        for (@NotNull final Task task : tasksRep) {
            if (task.getProjectId().equals(projectId)) {
                if (j == 1) {
                    serviceLocator.getTerminalService().print("List of tasks found by project ID:");
                }
                serviceLocator.getTerminalService().print(j + ". " + task.getName());
                j++;
            }
        }
        if (j == 1) {
            serviceLocator.getTerminalService().print("Attached tasks not found.");
        }
    }
}
