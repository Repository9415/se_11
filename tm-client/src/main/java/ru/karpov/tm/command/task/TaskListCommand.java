package ru.karpov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.AbstractException_Exception;
import ru.karpov.tm.api.endpoint.ITaskEndpoint;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.api.endpoint.Task;
import ru.karpov.tm.command.AbstractCommand;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

@NoArgsConstructor
public final class TaskListCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws AbstractException_Exception, Exception {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final ITaskEndpoint taskService = serviceLocator.getTaskEndpoint();
        @NotNull final String userId = session.getUserId();
        @NotNull final Collection<@NotNull Task> tasksRep = taskService.findAllUserTasks(session, userId);
        if (tasksRep.isEmpty()) {
            serviceLocator.getTerminalService().print("Tasks do not exist.");
            return;
        }

        serviceLocator.getTerminalService().print("1 - in the order of addition;");
        serviceLocator.getTerminalService().print("2 - by start date;");
        serviceLocator.getTerminalService().print("3 - by end date;");
        serviceLocator.getTerminalService().print("4 - by status.");
        serviceLocator.getTerminalService().print("Enter a number to select the sort order:");
        @NotNull final String numberSort = serviceLocator.getTerminalService().input();
        switch (numberSort) {
            case "":
            case "1":
                sortOrderByAdded(tasksRep);
                break;
            case "2":
                sortOrderByStartDate(tasksRep);
                break;
            case "3":
                sortOrderByFinishDate(tasksRep);
                break;
            case "4":
                sortOrderByStatus(tasksRep);
                break;
            default:
                serviceLocator.getTerminalService().print("Incorrect number entered. Try again!");
        }
    }

    void sortOrderByAdded(@NotNull final Collection<Task> tasksRep) {
        serviceLocator.getTerminalService().print("List of tasks:");
        printTasks(tasksRep);
    }

    void sortOrderByStartDate(@NotNull final Collection<Task> tasksRep) {
        serviceLocator.getTerminalService().print("List of tasks:");
        @NotNull final Collection<Task> tasks = tasksRep.stream()
                .filter(t -> t.getDateStart() != null)
                .sorted(Comparator.comparing(Task::getDateStart, XMLGregorianCalendar::compare))
                .collect(Collectors.toList());
        printTasks(tasks);
    }

    void sortOrderByFinishDate(@NotNull final Collection<Task> tasksRep) {
        serviceLocator.getTerminalService().print("List of tasks:");
        @NotNull final Collection<Task> tasks = tasksRep.stream()
                .filter(t -> t.getDateFinish() != null)
                .sorted(Comparator.comparing(Task::getDateFinish, XMLGregorianCalendar::compare).reversed())
                .collect(Collectors.toList());
        printTasks(tasks);
    }

    void sortOrderByStatus(@NotNull final Collection<Task> tasksRep) {
        serviceLocator.getTerminalService().print("List of tasks:");
        @NotNull final Collection<Task> tasks = tasksRep.stream()
                .filter(t -> t.getStatus() != null)
                .sorted(Comparator.comparing(Task::getStatus))
                .collect(Collectors.toList());
        printTasks(tasks);
    }

    void printTasks(@NotNull final Collection<Task> tasksRep) {
        int j = 1;
        for (@NotNull final Task task : tasksRep) {
            serviceLocator.getTerminalService().print(j + ". " + task.getName());
            j++;
        }
    }
}
