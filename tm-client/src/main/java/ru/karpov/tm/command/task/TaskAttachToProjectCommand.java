package ru.karpov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karpov.tm.api.endpoint.*;
import ru.karpov.tm.command.AbstractCommand;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

@NoArgsConstructor
public final class TaskAttachToProjectCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "attach-task-to-project";
    }

    @Override
    public String getDescription() {
        return "Attach tasks to project.";
    }

    @Override
    public void execute() throws IOException, AbstractException_Exception, Exception_Exception {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final String userId = session.getUserId();

        if (userId == null || userId.isEmpty()) {
            serviceLocator.getTerminalService().print("Parameter \"userId\" missing.");
            return;
        }
        serviceLocator.getTerminalService().print("Enter name of project for attach to task.");
        @NotNull final String attachNameProject = serviceLocator.getTerminalService().input().toLowerCase();
        serviceLocator.getTerminalService().print("Enter name of task for attach to project.");
        @NotNull final String attachNameTask = serviceLocator.getTerminalService().input().toLowerCase();
        final boolean isCheckNameProject = attachNameProject == null || attachNameProject.isEmpty();
        final boolean isCheckNameTask = attachNameTask == null || attachNameTask.isEmpty();
        if (isCheckNameProject || isCheckNameTask) {
            serviceLocator.getTerminalService().print("Incorrect name entered. Try again!");
            return;
        }
        final boolean isExistsNameProject = projectEndpoint.getAllProjectNames(session, userId).contains(attachNameProject);
        final boolean isExistsNameTask = taskEndpoint.getAllTaskNames(session, userId).contains(attachNameTask);
        if (isExistsNameProject && isExistsNameTask) {
            @Nullable Project projectForAttach = null;
            @NotNull final Iterator<Project> iterProject = projectEndpoint.findAllProjects(session, userId).iterator();
            while (iterProject.hasNext()) {
                @NotNull final Project project = iterProject.next();
                if (project.getName().equalsIgnoreCase(attachNameProject)) {
                    projectForAttach = project;
                    break;
                }
            }

            @NotNull final Collection<Task> tasksRep = taskEndpoint.findAllUserTasks(session, userId);
            @NotNull final Iterator<Task> iterTask = tasksRep.iterator();
            while (iterTask.hasNext()) {
                @NotNull final Task task = iterTask.next();
                if (task.getName().equals(attachNameTask)) {
                    task.setProjectId(projectForAttach.getId());
                    serviceLocator.getTaskEndpoint().mergeTask(session, task);
                    break;
                }
            }
            serviceLocator.getTerminalService().print("Task attached to project!");
            return;
        }
        serviceLocator.getTerminalService().print("Project not exist.");
    }
}
