package ru.karpov.tm.command.user;

import lombok.NoArgsConstructor;
import ru.karpov.tm.api.endpoint.AbstractException_Exception;
import ru.karpov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UserSignOutCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "user-sign-out";
    }

    @Override
    public String getDescription() {
        return "Sign out from Task Manager.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        serviceLocator.getSessionEndpoint().removeSession(serviceLocator.getSession());
        serviceLocator.setCurrentUser(null);
        serviceLocator.setSession(null);
        serviceLocator.getTerminalService().print("You are out of your profile.");
    }
}
