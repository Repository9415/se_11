package ru.karpov.tm.command.user;

import lombok.NoArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.AbstractException_Exception;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.api.endpoint.User;
import ru.karpov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UserSignInCommand extends AbstractCommand {
    final boolean secure = true;

    @Override
    public String getCommandName() {
        return "user-sign-in";
    }

    @Override
    public String getDescription() {
        return "Sign in to Task Manager.";
    }

    @Override
    public void execute() throws Exception, AbstractException_Exception {
        if (serviceLocator.getCurrentUser() != null) {
            serviceLocator.getTerminalService().print("Command is not available. You are already in the system.");
            return;
        }

        serviceLocator.getTerminalService().print("Enter your login:");
        @NotNull final String login = serviceLocator.getTerminalService().input();
        serviceLocator.getTerminalService().print("Enter your password:");
        @NotNull final String password = serviceLocator.getTerminalService().input();
        if (login == null || login.isEmpty() || password == null || password.isEmpty()) {
            serviceLocator.getTerminalService().print("Incorrect login or password entered! Try again.");
            return;
        }

        final boolean isExistLogin = serviceLocator.getUserEndpoint().containsUser(login);
        if (isExistLogin == false) {
            serviceLocator.getTerminalService().print("User not exist.");
            return;
        }

        @NotNull final Session session = serviceLocator.getSessionEndpoint().createSession(login, password);
        @NotNull final User user = serviceLocator.getUserEndpoint().findOneUser(session, login);
        final boolean checkPassword = user.getPassword().equals(DigestUtils.md5Hex(password));
        if (user != null && checkPassword) {
            serviceLocator.setCurrentUser(user);
            serviceLocator.setSession(session);
            serviceLocator.getTerminalService().print("You have successfully logged in!");
            return;
        } else {
            serviceLocator.getTerminalService().print("You entered the wrong password!");
        }
    }

    @Override
    public boolean isAllowed() {
        return this.secure;
    }
}
