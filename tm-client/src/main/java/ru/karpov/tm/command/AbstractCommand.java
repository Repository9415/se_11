package ru.karpov.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.ServiceLocator;
import ru.karpov.tm.api.endpoint.AbstractException_Exception;

public abstract class AbstractCommand {
    @NotNull
    protected ServiceLocator serviceLocator;
    protected boolean secure = false;

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public abstract String getCommandName();

    @NotNull
    public abstract String getDescription();

    @NotNull
    public abstract void execute() throws AbstractException_Exception, Exception;

    @NotNull
    public boolean isAllowed() {
        return secure;
    }
}
