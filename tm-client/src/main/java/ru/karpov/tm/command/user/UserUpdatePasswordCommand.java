package ru.karpov.tm.command.user;

import lombok.NoArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.AbstractException_Exception;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.api.endpoint.User;
import ru.karpov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UserUpdatePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getCommandName() {
        return "user-update-password";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User password update.";
    }

    @Override
    public void execute() throws Exception, AbstractException_Exception {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final User user = serviceLocator.getCurrentUser();
        serviceLocator.getTerminalService().print("Enter new password:");
        @NotNull final String password = serviceLocator.getTerminalService().input();
        if (password == null || password.isEmpty()) {
            serviceLocator.getTerminalService().print("Incorrect password entered! Try again.");
            return;
        }
        user.setPassword(DigestUtils.md5Hex(password));
        serviceLocator.getUserEndpoint().mergeUser(session, user);
        serviceLocator.getTerminalService().print("Password changed!");
    }
}
