package ru.karpov.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.Role;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class DataBinaryLoadBySerializationCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "data-binary-load";
    }

    @Override
    public String getDescription() {
        return "Load data from binary file by serialization.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Session session = serviceLocator.getSession();
        serviceLocator.getDomainEndpoint().dataBinaryLoadBySerialization(session);
        serviceLocator.getTerminalService().print("Load data by serialization finished.");
    }
}
