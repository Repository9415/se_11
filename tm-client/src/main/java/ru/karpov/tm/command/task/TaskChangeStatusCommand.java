package ru.karpov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.*;
import ru.karpov.tm.command.AbstractCommand;

import java.lang.Exception;
import java.util.Collection;

@NoArgsConstructor
public class TaskChangeStatusCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "task-change-status";
    }

    @Override
    public String getDescription() {
        return "Change the status of a task.";
    }

    @Override
    public void execute() throws Exception, AbstractException_Exception {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final String userId = session.getUserId();
        @NotNull final Collection<@NotNull Task> tasksRep = taskEndpoint.findAllUserTasks(session, userId);
        if (tasksRep.isEmpty()) {
            serviceLocator.getTerminalService().print("Tasks do not exist.");
            return;
        }
        serviceLocator.getTerminalService().print("Enter name of the task to change status:");
        @NotNull final String nameTask = serviceLocator.getTerminalService().input();
        if (nameTask == null || nameTask.isEmpty()) {
            serviceLocator.getTerminalService().print("Incorrect name task entered. Try again!");
            return;
        }
        if (taskEndpoint.getAllTaskNames(session, userId).contains(nameTask)) {
            serviceLocator.getTerminalService().print("Enter a number, what status a set?");
            serviceLocator.getTerminalService().print("1 - planned;");
            serviceLocator.getTerminalService().print("2 - in process;");
            serviceLocator.getTerminalService().print("3 - done;");
            int numberStatus = Integer.parseInt(serviceLocator.getTerminalService().input());
            switch (numberStatus) {
                case 1:
                    setStatus(Status.PLANNED, tasksRep, nameTask);
                    break;
                case 2:
                    setStatus(Status.IN_PROCESS, tasksRep, nameTask);
                    break;
                case 3:
                    setStatus(Status.COMPLETED, tasksRep, nameTask);
                    break;
                default:
                    serviceLocator.getTerminalService().print("Incorrect number entered. Try again!");
            }
        } else {
            serviceLocator.getTerminalService().print("Task does not exists!");
            return;
        }
        serviceLocator.getTerminalService().print("Task status changed!");
    }

    private void setStatus(@NotNull final Status status,
                           @NotNull final Collection<Task> tasksRep,
                           @NotNull final String nameTask) {
        for (@NotNull final Task task : tasksRep) {
            if (task.getName().equalsIgnoreCase((nameTask))) {
                task.setStatus(status);
            }
        }
    }
}