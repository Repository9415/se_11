package ru.karpov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.*;
import ru.karpov.tm.command.AbstractCommand;

import java.lang.Exception;
import java.util.Collection;

@NoArgsConstructor
public class ProjectChangeStatusCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "project-change-status";
    }

    @Override
    public String getDescription() {
        return "Change the status of a project.";
    }

    @Override
    public void execute() throws Exception, AbstractException_Exception {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final String userId = session.getUserId();
        @NotNull final Collection<@NotNull Project> projectsRep = projectEndpoint.findAllProjects(session, userId);
        if (projectsRep.isEmpty()) {
            serviceLocator.getTerminalService().print("Projects do not exist.");
            return;
        }
        serviceLocator.getTerminalService().print("Enter name of the project to change status:");
        @NotNull final String nameProject = serviceLocator.getTerminalService().input();
        if (nameProject == null || nameProject.isEmpty()) {
            serviceLocator.getTerminalService().print("Incorrect name project entered. Try again!");
            return;
        }
        if (projectEndpoint.getAllProjectNames(session, userId).contains(nameProject)) {
            serviceLocator.getTerminalService().print("Enter a number, what status a set?");
            serviceLocator.getTerminalService().print("1 - planned;");
            serviceLocator.getTerminalService().print("2 - in process;");
            serviceLocator.getTerminalService().print("3 - done;");
            int numberStatus = Integer.parseInt(serviceLocator.getTerminalService().input());
            switch (numberStatus) {
                case 1:
                    setStatus(Status.PLANNED, projectsRep, nameProject);
                    break;
                case 2:
                    setStatus(Status.IN_PROCESS, projectsRep, nameProject);
                    break;
                case 3:
                    setStatus(Status.COMPLETED, projectsRep, nameProject);
                    break;
                default:
                    serviceLocator.getTerminalService().print("Incorrect number entered. Try again!");
            }
        } else {
            serviceLocator.getTerminalService().print("Project does not exists!");
            return;
        }
        serviceLocator.getTerminalService().print("Project status changed!");
    }

    private void setStatus(@NotNull final Status status,
                           @NotNull final Collection<Project> projectsRep,
                           @NotNull final String nameProject) {
        for (@NotNull final Project project : projectsRep) {
            if (project.getName().equalsIgnoreCase((nameProject))) {
                project.setStatus(status);
            }
        }
    }
}
