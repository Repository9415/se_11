package ru.karpov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.*;
import ru.karpov.tm.command.AbstractCommand;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

@NoArgsConstructor
public final class TaskRemoveCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws AbstractException_Exception, IOException, Exception_Exception {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final String userId = session.getUserId();
        @NotNull final Collection<Task> tasksRep = taskEndpoint.findAllUserTasks(session, userId);

        serviceLocator.getTerminalService().print("Enter name of the task to remove:");
        @NotNull final String nameTask = serviceLocator.getTerminalService().input();
        if (nameTask == null || nameTask.isEmpty()) {
            serviceLocator.getTerminalService().print("Incorrect name task entered. Try again!");
            return;
        }
        @NotNull final Iterator iterTask = tasksRep.iterator();
        while (iterTask.hasNext()) {
            @NotNull final Task task = (Task) iterTask.next();
            if (task.getName().equalsIgnoreCase(nameTask)) {
                serviceLocator.getTerminalService().print("Task " + task.getName() + " removed.");
                taskEndpoint.removeTask(session, task.getId());
                return;
            }
        }
        serviceLocator.getTerminalService().print("Task not found.");
    }
}