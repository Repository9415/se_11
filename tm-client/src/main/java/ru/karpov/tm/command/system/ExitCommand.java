package ru.karpov.tm.command.system;

import lombok.NoArgsConstructor;
import ru.karpov.tm.api.endpoint.AbstractException_Exception;
import ru.karpov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class ExitCommand extends AbstractCommand {
    final boolean secure = true;

    @Override
    public String getCommandName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit from program.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        if (serviceLocator.getSession() != null) {
            serviceLocator.getSessionEndpoint().removeSession(serviceLocator.getSession());
            serviceLocator.setCurrentUser(null);
            serviceLocator.setSession(null);
        }
        serviceLocator.getTerminalService().print("The program execution is completed.");
        System.exit(0);
    }

    @Override
    public boolean isAllowed() {
        return this.secure;
    }
}
