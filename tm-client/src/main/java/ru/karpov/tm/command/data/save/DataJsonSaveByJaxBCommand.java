package ru.karpov.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.command.AbstractCommand;


@NoArgsConstructor
public final class DataJsonSaveByJaxBCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "data-json-jaxb-save";
    }

    @Override
    public String getDescription() {
        return "Save data in json format by Jax-B.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Session session = serviceLocator.getSession();
        serviceLocator.getDomainEndpoint().dataJsonSaveByJaxB(session);
        serviceLocator.getTerminalService().print("Save data in json format by JAX-B finished.");
    }
}
