package ru.karpov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.*;
import ru.karpov.tm.command.AbstractCommand;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

@NoArgsConstructor
public final class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws AbstractException_Exception, IOException, Exception_Exception {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final String userId = session.getUserId();
        @NotNull final Collection<Project> projectsRep = projectEndpoint.findAllProjects(session, userId);

        serviceLocator.getTerminalService().print("Enter name of the project to remove:");
        @NotNull final String removeProject = serviceLocator.getTerminalService().input().toLowerCase();
        if (removeProject == null || removeProject.isEmpty()) {
            serviceLocator.getTerminalService().print("Project name entered incorrectly. Try again!");
            return;
        }
        @NotNull final Iterator iterProject = projectsRep.iterator();
        while (iterProject.hasNext()) {
            @NotNull final Project project = (Project) iterProject.next();
            if (project.getName().toLowerCase().equals(removeProject)) {
                removeAllTasksByProjectId(project.getId());
                serviceLocator.getTerminalService().print("Project " + project.getName() + " removed.");
                projectEndpoint.removeProject(session, project.getId());
                return;
            }
        }
        serviceLocator.getTerminalService().print("Project not found.");
    }

    private void removeAllTasksByProjectId(@NotNull final String projectId) throws AbstractException_Exception, Exception_Exception {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final String userId = session.getUserId();
        @NotNull final Collection<Task> tasksRep = taskEndpoint.findAllUserTasks(session, userId);

        @NotNull final Iterator iterTask = tasksRep.iterator();
        while (iterTask.hasNext()) {
            @NotNull final Task task = (Task) iterTask.next();
            if (task.getProjectId().equals(projectId)) {
                taskEndpoint.removeTask(session, task.getId());
                serviceLocator.getTerminalService().print("Task " + task.getName() + " removed.");
            }
        }
    }
}
