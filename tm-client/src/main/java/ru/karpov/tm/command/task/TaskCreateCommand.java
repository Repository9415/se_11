package ru.karpov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.AbstractException_Exception;
import ru.karpov.tm.api.endpoint.ITaskEndpoint;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.api.endpoint.Task;
import ru.karpov.tm.command.AbstractCommand;

import javax.xml.datatype.DatatypeConfigurationException;
import java.io.IOException;
import java.util.Date;

import static ru.karpov.tm.util.DateFormat.parseStringToXMLGregorian;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws AbstractException_Exception, IOException, DatatypeConfigurationException {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final String userId = session.getUserId();

        serviceLocator.getTerminalService().print("Enter task name:");
        @NotNull final String nameTask = serviceLocator.getTerminalService().input();
        if (nameTask == null || nameTask.isEmpty()) {
            serviceLocator.getTerminalService().print("Task with an empty name cannot be created. Try again.");
            return;
        }
        if (taskEndpoint.getAllTaskNames(session, userId).contains(nameTask)) {
            serviceLocator.getTerminalService().print("Task with the same name already exists. Try again!");
            return;
        }
        serviceLocator.getTerminalService().print("Enter task description:");
        @NotNull final String descriptionTask = serviceLocator.getTerminalService().input();
        serviceLocator.getTerminalService().print("Enter task start date, in the format \"dd.MM.yyyy\":");
        @NotNull final String dateStart = serviceLocator.getTerminalService().input();
        serviceLocator.getTerminalService().print("Enter task finish date, in the format \"dd.MM.yyyy\":");
        @NotNull final String dateFinish = serviceLocator.getTerminalService().input();
        final boolean isCheckDescriptionTask = descriptionTask == null || descriptionTask.isEmpty();
        final boolean isCheckdateStart = dateStart == null || dateStart.isEmpty();
        final boolean isCheckdateFinish = dateFinish == null || dateFinish.isEmpty();
        if (isCheckDescriptionTask || isCheckdateStart || isCheckdateFinish) {
            serviceLocator.getTerminalService().print("Data entered incorrectly. Try again!");
            return;
        }

        @NotNull final Task task = new Task();
        task.setName(nameTask);
        task.setDescription(descriptionTask);
        task.setDateStart(parseStringToXMLGregorian(dateStart));
        task.setDateFinish(parseStringToXMLGregorian(dateFinish));
        task.setUserId(userId);
        taskEndpoint.persistTask(session, task);
        serviceLocator.getTerminalService().print("Task created.");
    }
}
