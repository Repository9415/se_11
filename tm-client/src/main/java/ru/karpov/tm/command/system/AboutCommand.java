package ru.karpov.tm.command.system;

import com.jcabi.manifests.Manifests;
import lombok.NoArgsConstructor;
import ru.karpov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class AboutCommand extends AbstractCommand {
    final boolean secure = true;

    @Override
    public String getCommandName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Shows information about program.";
    }

    @Override
    public void execute() {
        serviceLocator.getTerminalService().print("Build number: " + Manifests.read("buildNumber"));
        serviceLocator.getTerminalService().print("Developer: " + Manifests.read("developer"));
    }

    @Override
    public boolean isAllowed() {
        return this.secure;
    }
}
