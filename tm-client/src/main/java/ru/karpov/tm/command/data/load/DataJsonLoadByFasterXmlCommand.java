package ru.karpov.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.Role;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.command.AbstractCommand;


@NoArgsConstructor
public final class DataJsonLoadByFasterXmlCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "data-json-faster-load";
    }

    @Override
    public String getDescription() {
        return "Load data from json file by FasterXml.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Session session = serviceLocator.getSession();
        serviceLocator.getDomainEndpoint().dataJsonLoadByFasterXml(session);
        serviceLocator.getTerminalService().print("Load data from json file by FasterXml finished.");
    }
}
