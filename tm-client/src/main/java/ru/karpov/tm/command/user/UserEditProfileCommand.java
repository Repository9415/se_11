package ru.karpov.tm.command.user;

import lombok.NoArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.AbstractException_Exception;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.api.endpoint.User;
import ru.karpov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UserEditProfileCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "user-edit-profile";
    }

    @Override
    public String getDescription() {
        return "Editing user profile.";
    }

    @Override
    public void execute() throws Exception, AbstractException_Exception {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final User user = serviceLocator.getCurrentUser();

        serviceLocator.getTerminalService().print("Editing your profile.");
        serviceLocator.getTerminalService().print("Enter new login:");
        @NotNull final String login = serviceLocator.getTerminalService().input();
        final boolean isExistLogin = serviceLocator.getUserEndpoint().containsUser(login);
        if (isExistLogin == true) {
            serviceLocator.getTerminalService().print("Such user already exists. Try again.");
            return;
        }
        serviceLocator.getTerminalService().print("Enter new password:");
        @NotNull final String password = serviceLocator.getTerminalService().input();
        if (login == null || login.isEmpty() || password == null || password.isEmpty()) {
            serviceLocator.getTerminalService().print("Incorrect login or password entered! Try again.");
            return;
        }
        user.setLogin(login);
        user.setPassword(DigestUtils.md5Hex(password));
        serviceLocator.getUserEndpoint().mergeUser(session, user);
        serviceLocator.getTerminalService().print("Editing profile completed.");
    }
}
