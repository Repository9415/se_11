package ru.karpov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.AbstractException_Exception;
import ru.karpov.tm.api.endpoint.ITaskEndpoint;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class TaskClearCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all user tasks.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final String userId = session.getUserId();
        taskEndpoint.removeAllUserTasks(session, userId);
        serviceLocator.getTerminalService().print("All user tasks cleared");
    }
}
