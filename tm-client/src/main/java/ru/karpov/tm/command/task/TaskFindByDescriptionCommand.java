package ru.karpov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.AbstractException_Exception;
import ru.karpov.tm.api.endpoint.ITaskEndpoint;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.api.endpoint.Task;
import ru.karpov.tm.command.AbstractCommand;

import java.util.Collection;

@NoArgsConstructor
public class TaskFindByDescriptionCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "task-find-desc";
    }

    @Override
    public String getDescription() {
        return "Find tasks by name or part of a description.";
    }

    @Override
    public void execute() throws Exception, AbstractException_Exception {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final String userId = session.getUserId();
        @NotNull final Collection<@NotNull Task> tasksRep = taskEndpoint.findAllUserTasks(session, userId);
        if (tasksRep.isEmpty()) {
            serviceLocator.getTerminalService().print("Tasks do not exist.");
            return;
        }
        serviceLocator.getTerminalService().print("Enter name or part of the description to find the task:");
        @NotNull final String nameTask = serviceLocator.getTerminalService().input();
        if (nameTask == null || nameTask.isEmpty()) {
            serviceLocator.getTerminalService().print("Incorrect name task entered. Try again!");
            return;
        }
        int j = 1;
        for (@NotNull final Task task : tasksRep) {
            boolean isExistsName = task.getName().contains(nameTask);
            boolean isExistsDesc = task.getDescription().contains(nameTask);
            if (isExistsName || isExistsDesc) {
                serviceLocator.getTerminalService().print(j++ + ". Name: " + task.getName() + "; Desc: " + task.getDescription());
            }
        }
        if (j == 1) {
            serviceLocator.getTerminalService().print("Search returned no results.");
        }
    }
}
