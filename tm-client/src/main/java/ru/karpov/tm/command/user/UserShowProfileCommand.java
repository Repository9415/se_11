package ru.karpov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.User;
import ru.karpov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UserShowProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getCommandName() {
        return "user-show-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show user profile.";
    }

    @Override
    public void execute() {
        @NotNull final User user = serviceLocator.getCurrentUser();
        serviceLocator.getTerminalService().print("Information about your profile:");
        serviceLocator.getTerminalService().print("Your ID: " + user.getId());
        serviceLocator.getTerminalService().print("Your login: " + user.getLogin());
        serviceLocator.getTerminalService().print("Your password: ********");
        serviceLocator.getTerminalService().print("Your status: " + user.getRole());
    }
}
