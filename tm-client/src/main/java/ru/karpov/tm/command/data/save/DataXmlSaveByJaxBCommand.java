package ru.karpov.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.command.AbstractCommand;


@NoArgsConstructor
public final class DataXmlSaveByJaxBCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "data-xml-jaxb-save";
    }

    @Override
    public String getDescription() {
        return "Save data in xml format by Jax-B.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Session session = serviceLocator.getSession();
        serviceLocator.getDomainEndpoint().dataXmlSaveByJaxB(session);
        serviceLocator.getTerminalService().print("Save data in xml format by JAX-B finished.");
    }
}
