package ru.karpov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.AbstractException_Exception;
import ru.karpov.tm.api.endpoint.IProjectEndpoint;
import ru.karpov.tm.api.endpoint.Project;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.command.AbstractCommand;

import java.util.Date;

import static ru.karpov.tm.util.DateFormat.parseStringToXMLGregorian;

@NoArgsConstructor
public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new projects.";
    }

    @Override
    public void execute() throws Exception, AbstractException_Exception {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final String userId = session.getUserId();

        serviceLocator.getTerminalService().print("Enter project name:");
        @NotNull final String nameProject = serviceLocator.getTerminalService().input();
        if (nameProject == null || nameProject.isEmpty()) {
            serviceLocator.getTerminalService().print("Project with an empty name cannot be created. Try again!");
            return;
        }
        if (projectEndpoint.getAllProjectNames(session, userId).contains(nameProject)) {
            serviceLocator.getTerminalService().print("Project with the same name already exists. Try again!");
            return;
        }
        serviceLocator.getTerminalService().print("Enter project description:");
        @NotNull final String descriptionProject = serviceLocator.getTerminalService().input();
        serviceLocator.getTerminalService().print("Enter project start date, in the format \"dd.MM.yyyy\":");
        @NotNull final String dateStart = serviceLocator.getTerminalService().input();
        serviceLocator.getTerminalService().print("Enter project finish date, in the format \"dd.MM.yyyy\":");
        @NotNull final String dateFinish = serviceLocator.getTerminalService().input();
        final boolean isCheckDescriptionProject = descriptionProject == null || descriptionProject.isEmpty();
        final boolean isCheckdateStart = dateStart == null || dateStart.isEmpty();
        final boolean isCheckdateFinish = dateFinish == null || dateFinish.isEmpty();
        if (isCheckDescriptionProject || isCheckdateStart || isCheckdateFinish) {
            serviceLocator.getTerminalService().print("Data entered incorrectly. Try again!");
            return;
        }

        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(nameProject);
        project.setDateStart(parseStringToXMLGregorian(dateStart));
        project.setDateFinish(parseStringToXMLGregorian(dateFinish));
        projectEndpoint.persistProject(session, project);
        serviceLocator.getTerminalService().print("Project created.");
    }
}
