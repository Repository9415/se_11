package ru.karpov.tm.command.user;

import lombok.NoArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.AbstractException_Exception;
import ru.karpov.tm.api.endpoint.Role;
import ru.karpov.tm.api.endpoint.User;
import ru.karpov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UserRegistrationCommand extends AbstractCommand {
    final boolean secure = true;

    @Override
    public String getCommandName() {
        return "user-registration";
    }

    @Override
    public String getDescription() {
        return "User registration.";
    }

    @Override
    public void execute() throws Exception, AbstractException_Exception {
        if (serviceLocator.getCurrentUser() != null) {
            serviceLocator.getTerminalService().print("Command is not available. You are already in the system.");
            return;
        }

        serviceLocator.getTerminalService().print("Enter username:");
        @NotNull final String login = serviceLocator.getTerminalService().input();
        serviceLocator.getTerminalService().print("Enter password:");
        @NotNull final String password = serviceLocator.getTerminalService().input();
        if (login == null || login.isEmpty() || password == null || password.isEmpty()) {
            serviceLocator.getTerminalService().print("Incorrect login or password entered! Try again.");
            return;
        }

        final boolean isExistLogin = serviceLocator.getUserEndpoint().containsUser(login);
        if (isExistLogin == true) {
            serviceLocator.getTerminalService().print("Such user already exists. Try again.");
            return;
        }
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPassword(DigestUtils.md5Hex(password));
        user.setRole(Role.USER);
        serviceLocator.getUserEndpoint().persistUser(user);
        serviceLocator.getTerminalService().print("Registration completed successfully!");
    }

    @Override
    public boolean isAllowed() {
        return this.secure;
    }
}
