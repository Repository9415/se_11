package ru.karpov.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.Role;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.command.AbstractCommand;


@NoArgsConstructor
public final class DataXmlLoadByJaxBCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "data-xml-jaxb-load";
    }

    @Override
    public String getDescription() {
        return "Save data in xml format by Jax-B.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Session session = serviceLocator.getSession();
        serviceLocator.getDomainEndpoint().dataXmlLoadByFasterXml(session);
        serviceLocator.getTerminalService().print("Load data from xml by jaxb finished.");
    }
}
