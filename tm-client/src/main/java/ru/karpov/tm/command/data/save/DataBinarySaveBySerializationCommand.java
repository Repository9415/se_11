package ru.karpov.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class DataBinarySaveBySerializationCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "data-binary-save";
    }

    @Override
    public String getDescription() {
        return "Save data in binary file by serialization.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Session session = serviceLocator.getSession();
        serviceLocator.getDomainEndpoint().dataBinarySaveBySerialization(session);
        serviceLocator.getTerminalService().print("Save data by serialization finished.");
    }
}
