package ru.karpov.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.command.AbstractCommand;


@NoArgsConstructor
public final class DataJsonSaveByFasterXmlCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "data-json-faster-save";
    }

    @Override
    public String getDescription() {
        return "Save data in json format by FasterXml.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Session session = serviceLocator.getSession();
        serviceLocator.getDomainEndpoint().dataJsonSaveByFasterXml(session);
        serviceLocator.getTerminalService().print("Save data in json format by FasterXml finished.");
    }
}
