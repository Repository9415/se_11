package ru.karpov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class HelpCommand extends AbstractCommand {
    final boolean secure = true;

    @Override
    public String getCommandName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for (@NotNull final AbstractCommand command : this.serviceLocator.getCommands()) {
            serviceLocator.getTerminalService().print(command.getCommandName() + ": " + command.getDescription());
        }
    }

    @Override
    public boolean isAllowed() {
        return this.secure;
    }
}