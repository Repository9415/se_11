package ru.karpov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.AbstractException_Exception;
import ru.karpov.tm.api.endpoint.IProjectEndpoint;
import ru.karpov.tm.api.endpoint.Project;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.command.AbstractCommand;

import java.util.Collection;

@NoArgsConstructor
public class ProjectFindByDescriptionCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "project-find-desc";
    }

    @Override
    public String getDescription() {
        return "Find projects by name or part of a description.";
    }

    @Override
    public void execute() throws Exception, AbstractException_Exception {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final String userId = session.getUserId();
        @NotNull final Collection<@NotNull Project> projectsRep = projectEndpoint.findAllProjects(session, userId);
        if (projectsRep.isEmpty()) {
            serviceLocator.getTerminalService().print("Projects do not exist.");
            return;
        }
        serviceLocator.getTerminalService().print("Enter name or part of the description to find the project:");
        @NotNull final String nameProject = serviceLocator.getTerminalService().input();
        if (nameProject == null || nameProject.isEmpty()) {
            serviceLocator.getTerminalService().print("Incorrect name project entered. Try again!");
            return;
        }
        int j = 1;
        for (@NotNull final Project project : projectsRep) {
            boolean isExistsName = project.getName().contains(nameProject);
            boolean isExistsDesc = project.getDescription().contains(nameProject);
            if (isExistsName || isExistsDesc) {
                serviceLocator.getTerminalService().print(j++ + ". Name: " + project.getName() +
                        "; Desc: " + project.getDescription());
            }
        }
        if (j == 1) {
            serviceLocator.getTerminalService().print("Search returned no results.");
        }
    }
}
