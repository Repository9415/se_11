package ru.karpov.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.endpoint.Session;
import ru.karpov.tm.command.AbstractCommand;


@NoArgsConstructor
public final class DataXmlSaveByFasterXmlCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "data-xml-faster-save";
    }

    @Override
    public String getDescription() {
        return "Save data in xml format by FasterXml.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Session session = serviceLocator.getSession();
        serviceLocator.getDomainEndpoint().dataXmlSaveByFasterXml(session);
        serviceLocator.getTerminalService().print("Save data in xml format by FasterXml finished.");
    }
}
