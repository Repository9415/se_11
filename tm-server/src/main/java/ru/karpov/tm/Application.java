package ru.karpov.tm;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.context.Bootstrap;

public final class Application {
    public static void main(@NotNull final String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
