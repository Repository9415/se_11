package ru.karpov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.ServiceLocator;
import ru.karpov.tm.entity.Project;
import ru.karpov.tm.entity.Task;
import ru.karpov.tm.entity.User;
import ru.karpov.tm.exception.AbstractException;
import ru.karpov.tm.exception.AccessForbiddenException;

import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@XmlAccessorOrder
@NoArgsConstructor
@XmlRootElement(name = "domain")
//@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName(value = "domain")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Domain implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotNull
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    @JacksonXmlProperty(localName = "project")
    @JacksonXmlElementWrapper(localName = "projects")
    private List<Project> projects = new LinkedList<>();

    @NotNull
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    @JacksonXmlProperty(localName = "task")
    @JacksonXmlElementWrapper(localName = "tasks")
    private List<Task> tasks = new LinkedList<>();

    @NotNull
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    @JacksonXmlProperty(localName = "user")
    @JacksonXmlElementWrapper(localName = "users")
    private List<User> users = new LinkedList<>();

    @NotNull
    public List<Project> getProjects() {
        return projects;
    }

    @NotNull
    public List<Task> getTasks() {
        return tasks;
    }

    @NotNull
    public List<User> getUser() {
        return users;
    }

    public void load(@NotNull final ServiceLocator serviceLocator, @NotNull final String userId) throws AbstractException {
        if (serviceLocator == null) throw new AccessForbiddenException();
        this.projects = new LinkedList<>(serviceLocator.getProjectService().findAll(userId));
        this.tasks = new LinkedList<>(serviceLocator.getTaskService().findAll(userId));
        this.users = new LinkedList<>(serviceLocator.getUserService().findAll());
    }
}

