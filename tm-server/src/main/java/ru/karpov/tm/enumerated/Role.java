package ru.karpov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {
    USER("User"),
    ADMIN("Admin");

    @NotNull
    private String roleType;

    Role(@NotNull final String roleType) {
        this.roleType = roleType;
    }

    @NotNull
    public String displayName() {
        return roleType;
    }
}
