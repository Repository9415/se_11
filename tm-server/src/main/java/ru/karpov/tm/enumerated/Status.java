package ru.karpov.tm.enumerated;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;

@AllArgsConstructor
public enum Status {
    PLANNED("Planned."),
    IN_PROCESS("In the process."),
    COMPLETED("Completed.");

    @NotNull
    private final String name;

    @NotNull
    public String displayName() {
        return name;
    }
}
