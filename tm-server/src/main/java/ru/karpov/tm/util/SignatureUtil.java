package ru.karpov.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karpov.tm.constant.Constant;

public class SignatureUtil {

    @Nullable
    public static String sign(@Nullable final Object value) {
        if (value == null) return null;
        try {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writeValueAsString(value);
            return signJson(json);
        } catch (final JsonProcessingException e) {
            return null;
        }
    }

    @Nullable
    private static String signJson(@Nullable final String string) {
        if (string == null || string.isEmpty()) return null;
        @Nullable String result = string;
        for (int i = 0; i < Constant.COUNT_CYCLE; i++) {
            result = DigestUtils.md5Hex(Constant.SALT + result + Constant.SALT);
        }
        return result;
    }
}

