package ru.karpov.tm.util;

import org.jetbrains.annotations.NotNull;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormat {

    @NotNull
    private final static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    @NotNull
    public static Date parseDateFromString(@NotNull final String stringDate) {
        return dateFormatter.parse(stringDate, new ParsePosition(0));
    }

    @NotNull
    public static String parseDateToString(@NotNull final Date date) {
        return dateFormatter.format(date);
    }
}
