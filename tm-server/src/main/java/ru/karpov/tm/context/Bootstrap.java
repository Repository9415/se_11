package ru.karpov.tm.context;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karpov.tm.api.ServiceLocator;
import ru.karpov.tm.api.endpoint.*;
import ru.karpov.tm.api.repository.IProjectRepository;
import ru.karpov.tm.api.repository.ISessionRepository;
import ru.karpov.tm.api.repository.ITaskRepository;
import ru.karpov.tm.api.repository.IUserRepository;
import ru.karpov.tm.api.service.IProjectService;
import ru.karpov.tm.api.service.ISessionService;
import ru.karpov.tm.api.service.ITaskService;
import ru.karpov.tm.api.service.IUserService;
import ru.karpov.tm.endpoint.*;
import ru.karpov.tm.entity.Session;
import ru.karpov.tm.entity.User;
import ru.karpov.tm.enumerated.Role;
import ru.karpov.tm.repository.ProjectRepository;
import ru.karpov.tm.repository.SessionRepository;
import ru.karpov.tm.repository.TaskRepository;
import ru.karpov.tm.repository.UserRepository;
import ru.karpov.tm.service.ProjectService;
import ru.karpov.tm.service.SessionService;
import ru.karpov.tm.service.TaskService;
import ru.karpov.tm.service.UserService;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ITaskRepository tasks = new TaskRepository();

    @NotNull
    private final IProjectRepository projects = new ProjectRepository();

    @NotNull
    private final IUserRepository users = new UserRepository();

    @NotNull
    private final ISessionRepository sessions = new SessionRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(tasks);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projects);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(users);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessions);

    @Setter
    @Getter
    @Nullable
    private Session session = null;

    @NotNull private ITaskEndpoint taskEndpoint = new TaskEndpoint(this);
    @NotNull private IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);
    @NotNull private IUserEndpoint userEndpoint = new UserEndpoint(this);
    @NotNull private ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);
    @NotNull private IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    public void init() throws Exception {
        initDefaultUsers();
        Endpoint.publish("http://127.0.0.1:8080/task?wsdl", taskEndpoint);
        Endpoint.publish("http://127.0.0.1:8080/project?wsdl", projectEndpoint);
        Endpoint.publish("http://127.0.0.1:8080/user?wsdl", userEndpoint);
        Endpoint.publish("http://127.0.0.1:8080/session?wsdl", sessionEndpoint);
        Endpoint.publish("http://127.0.0.1:8080/domain?wsdl", domainEndpoint);
    }

    private void initDefaultUsers() throws Exception {
        userService.persist(new User("user", DigestUtils.md5Hex("user"), Role.USER));
        userService.persist(new User("admin", DigestUtils.md5Hex("admin"), Role.ADMIN));
    }
}