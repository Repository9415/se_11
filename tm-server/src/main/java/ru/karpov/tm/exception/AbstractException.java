package ru.karpov.tm.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public abstract class AbstractException extends Exception {

    @NotNull
    protected String message = "Errors!";

    public AbstractException(@NotNull final String message) {
        super(message);
    }

    @NotNull
    @Override
    public String getMessage() {
        return this.message;
    }
}
