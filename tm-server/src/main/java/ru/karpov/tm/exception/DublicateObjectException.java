package ru.karpov.tm.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class DublicateObjectException extends AbstractException {

    @NotNull
    private String message = "Object already exist!";

    public DublicateObjectException(@NotNull final String message) {
        this.message = message;
    }

    @NotNull
    @Override
    public String getMessage() {
        return this.message;
    }
}
