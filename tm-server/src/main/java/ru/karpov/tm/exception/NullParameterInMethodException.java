package ru.karpov.tm.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class NullParameterInMethodException extends AbstractException {

    @NotNull
    private String message = "Parameter is null";

    public NullParameterInMethodException(@NotNull final String message) {
        this.message = message;
    }

    @NotNull
    @Override
    public String getMessage() {
        return this.message;
    }
}
