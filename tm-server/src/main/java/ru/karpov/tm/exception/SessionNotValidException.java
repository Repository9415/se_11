package ru.karpov.tm.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class SessionNotValidException extends AbstractException {

    @NotNull
    private String message = "Session is not valid!";

    public SessionNotValidException(@NotNull final String message) {
        this.message = message;
    }

    @NotNull
    @Override
    public String getMessage() {
        return this.message;
    }
}
