package ru.karpov.tm.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class IncorrectParseDateException extends AbstractException {

    @NotNull
    private String message = "Incorrect written date. Try again!";

    public IncorrectParseDateException(@NotNull final String message) {
        this.message = message;
    }

    @NotNull
    @Override
    public String getMessage() {
        return this.message;
    }
}
