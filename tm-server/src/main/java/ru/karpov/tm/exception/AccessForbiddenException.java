package ru.karpov.tm.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public class AccessForbiddenException extends AbstractException {

    @NotNull
    private String message = "Access denied";

    public AccessForbiddenException(@NotNull final String message) {
        this.message = message;
    }

    @NotNull
    @Override
    public String getMessage() {
        return this.message;
    }
}
