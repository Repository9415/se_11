package ru.karpov.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.ServiceLocator;
import ru.karpov.tm.api.endpoint.IDomainEndpoint;
import ru.karpov.tm.constant.Path;
import ru.karpov.tm.dto.Domain;
import ru.karpov.tm.entity.Project;
import ru.karpov.tm.entity.Session;
import ru.karpov.tm.entity.Task;
import ru.karpov.tm.exception.AbstractException;
import ru.karpov.tm.exception.AccessForbiddenException;

import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.karpov.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint implements IDomainEndpoint {

    @NotNull private ServiceLocator serviceLocator;

    public DomainEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void dataBinarySaveBySerialization(@NotNull final Session session) throws AbstractException, Exception {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionForAdmin(session);
        @NotNull final Domain domain = new Domain();
        domain.load(serviceLocator, session.getUserId());
        @NotNull final File savePath = new File(Path.BINARY_FILE);
        @NotNull final FileOutputStream outputStream = new FileOutputStream(savePath);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
    }

    @Override
    public void dataJsonSaveByFasterXml(@NotNull final Session session) throws AbstractException, Exception {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionForAdmin(session);
        @NotNull final Domain domain = new Domain();
        domain.load(serviceLocator, session.getUserId());
        @NotNull final File saveFile = new File(Path.JSON_FASTERXML_FILE);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(saveFile, domain);
    }

    @Override
    public void dataJsonSaveByJaxB(@NotNull final Session session) throws AbstractException, Exception {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionForAdmin(session);
        @NotNull final Domain domain = new Domain();
        domain.load(serviceLocator, session.getUserId());
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final File saveFile = new File(Path.JSON_JAXB_FILE);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, saveFile);
    }

    @Override
    public void dataXmlSaveByFasterXml(@NotNull final Session session) throws AbstractException, Exception {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionForAdmin(session);
        @NotNull final Domain domain = new Domain();
        domain.load(serviceLocator, session.getUserId());
        @NotNull final File saveFile = new File(Path.XML_FASTERXML_FILE);
        @NotNull final ObjectMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(saveFile, domain);
    }

    @Override
    public void dataXmlSaveByJaxB(@NotNull final Session session) throws Exception {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionForAdmin(session);
        @NotNull final Domain domain = new Domain();
        domain.load(serviceLocator, session.getUserId());
        @NotNull final File saveFile = new File(Path.XML_JAXB_FILE);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, saveFile);
    }

    @Override
    public void dataBinaryLoadBySerialization(@NotNull final Session session) throws AbstractException, Exception {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionForAdmin(session);
        @NotNull final File savePath = new File(Path.BINARY_FILE);
        @NotNull final FileInputStream inputStream = new FileInputStream(savePath);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        if (domain == null) throw new AccessForbiddenException();
        loadTasksBySerialization(domain.getTasks());
        loadProjectsBySerialization(domain.getProjects());
        objectInputStream.close();
    }

    @Override
    public void dataJsonLoadByFasterXml(@NotNull final Session session) throws AbstractException, Exception {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionForAdmin(session);
        @NotNull final File saveFile = new File(Path.JSON_FASTERXML_FILE);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final Domain domain = mapper.readValue(saveFile, Domain.class);
        if (domain == null) throw new AccessForbiddenException("Domain is null");
        loadTasks(domain.getTasks());
        loadProjects(domain.getProjects());
    }

    @Override
    public void dataJsonLoadByJaxB(@NotNull final Session session) throws AbstractException, Exception {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionForAdmin(session);
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final File readFile = new File(Path.JSON_JAXB_FILE);
        @NotNull final StreamSource sourceFile = new StreamSource(readFile);
        @NotNull final org.eclipse.persistence.jaxb.JAXBContext context = (org.eclipse.persistence.jaxb.JAXBContext) org.eclipse.persistence.jaxb.JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(sourceFile);
        if (domain == null) throw new AccessForbiddenException("Domain is null");
        loadTasks(domain.getTasks());
        loadProjects(domain.getProjects());
    }

    @Override
    public void dataXmlLoadByFasterXml(@NotNull final Session session) throws AbstractException, Exception {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionForAdmin(session);
        @NotNull final File saveFile = new File(Path.XML_FASTERXML_FILE);
        @NotNull final ObjectMapper mapper = new XmlMapper();
        @NotNull final Domain domain = mapper.readValue(saveFile, Domain.class);
        if (domain == null) throw new AccessForbiddenException("Domain is null");
        loadTasks(domain.getTasks());
        loadProjects(domain.getProjects());
    }

    @Override
    public void dataXmlLoadByJaxB(@NotNull final Session session) throws AbstractException, Exception {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionForAdmin(session);
        @NotNull final File readFile = new File(Path.XML_JAXB_FILE);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller marshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) marshaller.unmarshal(readFile);
        if (domain == null) throw new AccessForbiddenException("Domain is null");
        loadTasks(domain.getTasks());
        loadProjects(domain.getProjects());
    }

    private void loadTasksBySerialization(@NotNull final List<Task> tasks) throws AbstractException {
        for (@NotNull final Task task : tasks) {
            serviceLocator.getTaskService().merge(task);
        }
    }

    private void loadProjectsBySerialization(@NotNull final List<Project> projects) throws AbstractException {
        for (@NotNull final Project project : projects) {
            serviceLocator.getProjectService().merge(project);
        }
    }

    private void loadTasks(@NotNull final List<Task> tasks) throws AbstractException {
        for (@NotNull final Task task : tasks) {
            serviceLocator.getTaskService().merge(task);
        }
    }

    private void loadProjects(@NotNull final List<Project> projects) throws AbstractException {
        for (@NotNull final Project project : projects) {
            serviceLocator.getProjectService().merge(project);
        }
    }
}