package ru.karpov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karpov.tm.api.ServiceLocator;
import ru.karpov.tm.api.endpoint.IUserEndpoint;
import ru.karpov.tm.entity.Session;
import ru.karpov.tm.entity.User;
import ru.karpov.tm.exception.AbstractException;
import ru.karpov.tm.exception.AccessForbiddenException;

import javax.jws.WebService;
import java.util.Collection;

@NoArgsConstructor
@WebService(endpointInterface = "ru.karpov.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint implements IUserEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public User findOneUser(@NotNull final Session session, @NotNull final String login) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getUserService().findOne(login);
    }

    @NotNull
    @Override
    public Collection<User> findAllUsers(@NotNull final Session session) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getUserService().findAll();
    }

    @Override
    public void persistUser(@NotNull final User user) throws AbstractException {
        if (user == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getUserService().persist(user);
    }

    @Override
    public void mergeUser(@NotNull final Session session, @NotNull final User user) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getUserService().merge(user);
    }

    @Override
    public void removeUser(@NotNull final Session session, @NotNull final String login) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getUserService().remove(login);
    }

    @Override
    public boolean containsUser(@NotNull final String login) throws AbstractException {
        return serviceLocator.getUserService().contains(login);
    }
}
