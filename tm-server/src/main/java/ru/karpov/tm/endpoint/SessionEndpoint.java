package ru.karpov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karpov.tm.api.ServiceLocator;
import ru.karpov.tm.api.endpoint.ISessionEndpoint;
import ru.karpov.tm.entity.Session;
import ru.karpov.tm.entity.User;
import ru.karpov.tm.exception.AbstractException;
import ru.karpov.tm.exception.AccessForbiddenException;
import ru.karpov.tm.util.SignatureUtil;

import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.karpov.tm.api.endpoint.ISessionEndpoint")
public final class SessionEndpoint implements ISessionEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;

    public SessionEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public Session createSession(@NotNull final String login, @NotNull final String passwordHash) throws AbstractException {
        if (login == null || login.isEmpty())
            throw new AccessForbiddenException("Access denied. Session does not exist.");
        if (passwordHash == null || passwordHash.isEmpty())
            throw new AccessForbiddenException("Access denied. Session does not exist.");
        @NotNull final User user = serviceLocator.getUserService().findOne(login);
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        session.setSignature(SignatureUtil.sign(session));
        serviceLocator.setSession(session);
        serviceLocator.getSessionService().persist(session);
        return session;
    }

    @Override
    public void removeSession(@NotNull final Session session) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().remove(session.getId());
    }

    @NotNull
    @Override
    public Session findOneSession(@NotNull final Session session, @NotNull final String userId) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        return serviceLocator.getSessionService().findOne(userId);
    }
}
