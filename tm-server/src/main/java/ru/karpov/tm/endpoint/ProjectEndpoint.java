package ru.karpov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karpov.tm.api.ServiceLocator;
import ru.karpov.tm.api.endpoint.IProjectEndpoint;
import ru.karpov.tm.entity.Project;
import ru.karpov.tm.entity.Session;
import ru.karpov.tm.exception.AbstractException;
import ru.karpov.tm.exception.AccessForbiddenException;

import javax.jws.WebService;
import java.util.Collection;
import java.util.Set;

@NoArgsConstructor
@WebService(endpointInterface = "ru.karpov.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint implements IProjectEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public Project findOneProject(@NotNull final Session session, @NotNull final String projectId) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getProjectService().findOne(projectId);
    }

    @NotNull
    @Override
    public Collection<Project> findAllProjects(@NotNull final Session session, @NotNull final String userId) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getProjectService().findAll(userId);
    }

    @Override
    public void persistProject(@NotNull final Session session, @NotNull final Project project) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getProjectService().persist(project);
    }

    @Override
    public void mergeProject(@NotNull final Session session, @NotNull final Project project) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getProjectService().merge(project);
    }

    @Override
    public void removeProject(@NotNull final Session session, @NotNull final String projectId) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getProjectService().remove(projectId);
    }

    @Override
    public void removeAllProjects(@NotNull final Session session, @NotNull final String userId) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getProjectService().removeAll(userId);
    }

    @Override
    public boolean containsProject(@NotNull final Session session, @NotNull final String projectId) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getProjectService().contains(projectId);
    }

    @NotNull
    @Override
    public Set<String> getAllProjectNames(@NotNull final Session session, @NotNull final String userId) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getProjectService().getAllProjectNames(userId);
    }
}
