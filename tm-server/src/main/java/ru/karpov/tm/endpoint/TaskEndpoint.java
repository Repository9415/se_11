package ru.karpov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karpov.tm.api.ServiceLocator;
import ru.karpov.tm.api.endpoint.ITaskEndpoint;
import ru.karpov.tm.entity.Session;
import ru.karpov.tm.entity.Task;
import ru.karpov.tm.exception.AbstractException;
import ru.karpov.tm.exception.AccessForbiddenException;

import javax.jws.WebService;
import java.util.Collection;
import java.util.Set;

@NoArgsConstructor
@WebService(endpointInterface = "ru.karpov.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint implements ITaskEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public Task findOneTask(@NotNull final Session session, @NotNull final String taskId) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().findOne(taskId);
    }

    @NotNull
    @Override
    public Collection<Task> findAllUserTasks(@NotNull final Session session, @NotNull final String userId) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().findAll(userId);
    }

    @Override
    public void persistTask(@NotNull final Session session, @NotNull final Task task) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getTaskService().persist(task);
    }

    @Override
    public void mergeTask(@NotNull final Session session, @NotNull final Task task) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getTaskService().merge(task);
    }

    @Override
    public void removeTask(@NotNull final Session session, @NotNull final String taskId) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getTaskService().remove(taskId);
    }

    @Override
    public void removeAllUserTasks(@NotNull final Session session, @NotNull final String userId) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getTaskService().removeAll(userId);
    }

    @Override
    public boolean containsTask(@NotNull final Session session, @NotNull final String taskId) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().contains(taskId);
    }

    @NotNull
    @Override
    public Set<String> getAllTaskNames(@NotNull final Session session, @NotNull final String userId) throws AbstractException {
        if (session == null) throw new AccessForbiddenException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().getAllTaskNames(userId);
    }
}
