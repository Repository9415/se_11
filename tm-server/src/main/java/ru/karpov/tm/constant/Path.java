package ru.karpov.tm.constant;

public class Path {
    //    public static String SAVE_DIR = System.getProperty("user.dir") + File.separator + "taskmanager" + File.separator + "/data/data.bin";
    public static String BINARY_FILE = "./tm-server/data/data.bin";
    public static String XML_JAXB_FILE = "./tm-server/data/data_xml_jaxb.xml";
    public static String XML_FASTERXML_FILE = "./tm-server/data/data_xml_fasterXml.xml";
    public static String JSON_JAXB_FILE = "./tm-server/data/data_json_jaxb.json";
    public static String JSON_FASTERXML_FILE = "./tm-server/data/data_json_fasterXml.json";
}
