package ru.karpov.tm.constant;

import org.jetbrains.annotations.NotNull;

public class Constant {
    @NotNull public static String SALT = "brDtrg2q";
    @NotNull public static Integer COUNT_CYCLE = 2500;
    @NotNull public final static Integer SESSION_LIFE_TIME = 600000;
}
