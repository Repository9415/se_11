package ru.karpov.tm.entity;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.enumerated.Role;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public final class User extends AbstractEntity {

    @NotNull private String login = "";
    @NotNull private String password = "";
    @NotNull private Role role = Role.USER;
}
