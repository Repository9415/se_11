package ru.karpov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.karpov.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractEntity implements Cloneable {

    @Nullable
    String userId;

    @Nullable
    Role role;

    @Nullable
    @JsonIgnore
    String signature;

    //    @JsonIgnore
    final long timeStamp = System.currentTimeMillis();
}

