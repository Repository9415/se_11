package ru.karpov.tm.entity;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public final class Project extends AbstractEntity {

    @NotNull private String userId = "";
    @NotNull private String name = "Unnamed";
    @NotNull private String description = "";
    @NotNull private Date dateStart = new Date();
    @NotNull private Date dateFinish = new Date();
    @NotNull private Status status = Status.PLANNED;
}
