package ru.karpov.tm.entity;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.UUID;

public abstract class AbstractEntity implements Serializable {

    @Getter
    @NotNull
    @XmlElement
    protected final String id = UUID.randomUUID().toString();
}
