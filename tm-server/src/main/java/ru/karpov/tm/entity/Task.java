package ru.karpov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.enumerated.Status;

import java.util.Date;

@Setter
@Getter
@ToString
@NoArgsConstructor
public final class Task extends AbstractEntity {

    @NotNull private String userId = "";
    @NotNull private String projectId = "";
    @NotNull private String name = "Unnamed";
    @NotNull private String description = "";
    @NotNull private Date dateStart = new Date();
    @NotNull private Date dateFinish = new Date();
    @NotNull private Status status = Status.PLANNED;

    public Task(@NotNull final String name,
                @NotNull final String description,
                @NotNull final Date dateStart,
                @NotNull final Date dateFinish,
                @NotNull final String userId) {
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
        this.userId = userId;
    }
}

