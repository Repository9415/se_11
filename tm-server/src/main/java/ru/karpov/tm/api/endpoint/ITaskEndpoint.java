package ru.karpov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.entity.Session;
import ru.karpov.tm.entity.Task;
import ru.karpov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.Set;

@WebService
public interface ITaskEndpoint {

    @NotNull
    @WebMethod
    Task findOneTask(@NotNull @WebParam(name = "session") final Session session,
                     @NotNull @WebParam(name = "taskId") final String taskId
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Collection<Task> findAllUserTasks(@WebParam(name = "session") final Session session,
                                      @WebParam(name = "userId") final String userId
    ) throws AbstractException;

    @WebMethod
    void persistTask(@NotNull @WebParam(name = "session") final Session session,
                     @NotNull @WebParam(name = "task") final Task task
    ) throws AbstractException;

    @WebMethod
    void mergeTask(@NotNull @WebParam(name = "session") final Session session,
                   @NotNull @WebParam(name = "task") final Task task
    ) throws AbstractException;

    @WebMethod
    void removeTask(@NotNull @WebParam(name = "session") final Session session,
                    @NotNull @WebParam(name = "taskId") final String taskId
    ) throws AbstractException;

    @WebMethod
    void removeAllUserTasks(@WebParam(name = "session") final Session session,
                            @WebParam(name = "userId") final String userId
    ) throws AbstractException;

    @WebMethod
    boolean containsTask(@NotNull @WebParam(name = "session") final Session session,
                         @NotNull @WebParam(name = "taskId") final String taskId
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Set<String> getAllTaskNames(@WebParam(name = "session") final Session session,
                                @WebParam(name = "userId") final String userId
    ) throws AbstractException;
}
