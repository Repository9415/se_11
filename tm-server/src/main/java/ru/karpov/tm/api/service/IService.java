package ru.karpov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.entity.AbstractEntity;
import ru.karpov.tm.exception.AbstractException;

public interface IService<@NotNull T extends AbstractEntity> {
    @NotNull T findOne(@NotNull final String entityId) throws AbstractException;

    void persist(@NotNull final T entity) throws AbstractException;

    void remove(@NotNull final String entityId) throws AbstractException;

    boolean contains(@NotNull final String entityId) throws AbstractException;
}
