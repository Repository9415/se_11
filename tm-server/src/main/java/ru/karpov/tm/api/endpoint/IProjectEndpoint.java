package ru.karpov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.entity.Project;
import ru.karpov.tm.entity.Session;
import ru.karpov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.Set;

@WebService
public interface IProjectEndpoint {

    @NotNull
    @WebMethod
    Project findOneProject(@NotNull @WebParam(name = "session") final Session session,
                           @NotNull @WebParam(name = "projectId") final String projectId
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Collection<Project> findAllProjects(@WebParam(name = "session") final Session session,
                                        @WebParam(name = "userId") final String userId
    ) throws AbstractException;

    @WebMethod
    void persistProject(@NotNull @WebParam(name = "session") final Session session,
                        @NotNull @WebParam(name = "project") final Project project
    ) throws AbstractException;

    @WebMethod
    void mergeProject(@NotNull @WebParam(name = "session") final Session session,
                      @NotNull @WebParam(name = "project") final Project project
    ) throws AbstractException;

    @WebMethod
    void removeProject(@NotNull @WebParam(name = "session") final Session session,
                       @NotNull @WebParam(name = "projectId") final String projectId
    ) throws AbstractException;

    @WebMethod
    void removeAllProjects(@WebParam(name = "session") final Session session,
                           @WebParam(name = "userId") final String userId
    ) throws AbstractException;

    @WebMethod
    boolean containsProject(@NotNull @WebParam(name = "session") final Session session,
                            @NotNull @WebParam(name = "projectId") final String projectId
    ) throws AbstractException;

    @WebMethod
    Set<String> getAllProjectNames(@NotNull @WebParam(name = "session") final Session session,
                                   @NotNull @WebParam(name = "userId") final String userId
    ) throws AbstractException;
}
