package ru.karpov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karpov.tm.api.service.IProjectService;
import ru.karpov.tm.api.service.ISessionService;
import ru.karpov.tm.api.service.ITaskService;
import ru.karpov.tm.api.service.IUserService;
import ru.karpov.tm.entity.Session;
import ru.karpov.tm.entity.User;

public interface ServiceLocator {

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IUserService getUserService();

    @NotNull
    ISessionService getSessionService();

    void setSession(@Nullable final Session session);
}
