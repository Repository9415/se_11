package ru.karpov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.entity.Session;

import java.util.Collection;

public interface ISessionRepository {
    @NotNull Session findOne(@NotNull final String sessionId);

    @NotNull Session findOne(@NotNull final String userId, @NotNull final String sessionId);

    void remove(@NotNull final String userId, @NotNull final String id);

    @NotNull Collection<@NotNull Session> findAll();

    void persist(@NotNull final Session session);

    void merge(@NotNull final Session session);

    void remove(@NotNull final String sessionId);

    boolean contains(@NotNull final String sessionId);
}

