package ru.karpov.tm.api.service;

import ru.karpov.tm.exception.AbstractException;

public interface IDomainService {
    void dataBinarySaveBySerialization() throws AbstractException, Exception;

    void dataJsonSaveByFasterXml() throws AbstractException, Exception;

    void dataJsonSaveByJaxB() throws AbstractException, Exception;

    void dataXmlSaveByFasterXml() throws AbstractException, Exception;

    void dataXmlSaveByJaxB() throws AbstractException, Exception;

    void dataBinaryLoadBySerialization() throws AbstractException, Exception;

    void dataJsonLoadByFasterXml() throws AbstractException, Exception;

    void dataJsonLoadByJaxB() throws AbstractException, Exception;

    void dataXmlLoadByFasterXml() throws AbstractException, Exception;

    void dataXmlLoadByJaxB() throws AbstractException, Exception;
}
