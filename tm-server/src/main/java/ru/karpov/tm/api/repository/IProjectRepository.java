package ru.karpov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.entity.Project;

import java.util.Collection;

public interface IProjectRepository {
    @NotNull Project findOne(@NotNull final String projectId);

    @NotNull Collection<@NotNull Project> findAll(@NotNull final String userId);

    void persist(@NotNull final Project project);

    void merge(@NotNull final Project project);

    void remove(@NotNull final String projectId);

    void removeAll(@NotNull final String userId);

    boolean contains(@NotNull final String projectId);

//    @NotNull Map<String, Project> getAllEntities();
}
