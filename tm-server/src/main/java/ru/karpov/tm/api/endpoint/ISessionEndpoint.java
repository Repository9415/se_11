package ru.karpov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karpov.tm.entity.Session;
import ru.karpov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@WebService
public interface ISessionEndpoint {

    @Nullable
    @WebMethod
    Session findOneSession(@NotNull @WebParam(name = "session") final Session session,
                           @NotNull @WebParam(name = "userId") final String userId//,
                           //@NotNull @WebParam(name = "sessionId") final String sessionId
    ) throws AbstractException;

    @WebMethod
    void removeSession(@NotNull @WebParam(name = "session") final Session session) throws AbstractException;

    @Nullable
    @WebMethod
    Session createSession(@NotNull @WebParam(name = "login") final String login,
                          @NotNull @WebParam(name = "passwordHash") final String passwordHash
    ) throws AbstractException;
}
