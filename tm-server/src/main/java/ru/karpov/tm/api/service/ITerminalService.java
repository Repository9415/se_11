package ru.karpov.tm.api.service;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public interface ITerminalService {
    @NotNull String input() throws IOException;

    void print(@NotNull final String string);
}
