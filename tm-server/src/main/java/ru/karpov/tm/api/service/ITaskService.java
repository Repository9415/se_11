package ru.karpov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.entity.Task;
import ru.karpov.tm.exception.AbstractException;

import java.util.Collection;
import java.util.Set;

public interface ITaskService {
    @NotNull Task findOne(@NotNull final String taskId) throws AbstractException;

    @NotNull Collection<Task> findAll(@NotNull final String userId) throws AbstractException;

    void persist(@NotNull final Task task) throws AbstractException;

    void merge(@NotNull final Task task) throws AbstractException;

    void remove(@NotNull final String taskId) throws AbstractException;

    void removeAll(@NotNull final String userId) throws AbstractException;

    boolean contains(@NotNull final String taskId) throws AbstractException;

    @NotNull Set<String> getAllTaskNames(@NotNull final String userId) throws AbstractException;
}
