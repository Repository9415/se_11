package ru.karpov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.entity.AbstractEntity;

import java.util.Map;

public interface IRepository<@NotNull T extends AbstractEntity> {
    @NotNull T findOne(@NotNull final String keyMap);

    void persist(@NotNull final T entity);

    void merge(@NotNull final T entity);

    void remove(@NotNull final String entity);

    boolean contains(@NotNull final String keyMap);

    @NotNull Map<String, T> getAllEntities();
}