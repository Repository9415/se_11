package ru.karpov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karpov.tm.entity.Session;
import ru.karpov.tm.exception.AbstractException;

import java.util.Collection;

public interface ISessionService {
    @NotNull Session findOne(@NotNull final String sessionId) throws AbstractException;

    @NotNull Session findOne(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    void remove(@NotNull final String userId, @NotNull final String SessionId) throws AbstractException;

    @NotNull Collection<@NotNull Session> findAll() throws AbstractException;

    void persist(@NotNull final Session session) throws AbstractException;

    void merge(@NotNull final Session session) throws AbstractException;

    void remove(@NotNull final String sessionId) throws AbstractException;

    void validateSessionForAdmin(@NotNull final Session session) throws AbstractException;

    void validateSession(@NotNull final Session session) throws AbstractException;

    boolean contains(@NotNull final String sessionId) throws AbstractException;
}
















