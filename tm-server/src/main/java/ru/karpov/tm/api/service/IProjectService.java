package ru.karpov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.entity.Project;
import ru.karpov.tm.exception.AbstractException;

import java.util.Collection;
import java.util.Set;

public interface IProjectService {
    @NotNull Project findOne(@NotNull final String projectId) throws AbstractException;

    @NotNull Collection<Project> findAll(@NotNull final String userId) throws AbstractException;

    void persist(@NotNull final Project project) throws AbstractException;

    void merge(@NotNull final Project project) throws AbstractException;

    void remove(@NotNull final String projectId) throws AbstractException;

    void removeAll(@NotNull final String userId) throws AbstractException;

    boolean contains(@NotNull final String projectId) throws AbstractException;

    @NotNull Set<String> getAllProjectNames(@NotNull final String userId) throws AbstractException;
}
