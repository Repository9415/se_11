package ru.karpov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.entity.User;
import ru.karpov.tm.exception.AbstractException;

import java.util.Collection;

public interface IUserService {
    @NotNull User findOne(@NotNull final String login) throws AbstractException;

    @NotNull Collection<@NotNull User> findAll() throws AbstractException;

    void persist(@NotNull final User user) throws AbstractException;

    void merge(@NotNull final User user) throws AbstractException;

    void remove(@NotNull final String login) throws AbstractException;

    boolean contains(@NotNull final String login) throws AbstractException;
}











