package ru.karpov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.entity.Session;
import ru.karpov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {

    @WebMethod
    void dataBinarySaveBySerialization(@NotNull @WebParam(name = "session") final Session session) throws AbstractException, Exception;

    @WebMethod
    void dataJsonSaveByFasterXml(@NotNull @WebParam(name = "session") final Session session) throws AbstractException, Exception;

    @WebMethod
    void dataJsonSaveByJaxB(@NotNull @WebParam(name = "session") final Session session) throws AbstractException, Exception;

    @WebMethod
    void dataXmlSaveByFasterXml(@NotNull @WebParam(name = "session") final Session session) throws AbstractException, Exception;

    @WebMethod
    void dataXmlSaveByJaxB(@NotNull @WebParam(name = "session") final Session session) throws AbstractException, Exception;

    @WebMethod
    void dataBinaryLoadBySerialization(@NotNull @WebParam(name = "session") final Session session) throws AbstractException, Exception;

    @WebMethod
    void dataJsonLoadByFasterXml(@NotNull @WebParam(name = "session") final Session session) throws AbstractException, Exception;

    @WebMethod
    void dataJsonLoadByJaxB(@NotNull @WebParam(name = "session") final Session session) throws AbstractException, Exception;

    @WebMethod
    void dataXmlLoadByFasterXml(@NotNull @WebParam(name = "session") final Session session) throws AbstractException, Exception;

    @WebMethod
    void dataXmlLoadByJaxB(@NotNull @WebParam(name = "session") final Session session) throws AbstractException, Exception;
}
