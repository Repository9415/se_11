package ru.karpov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.entity.Session;
import ru.karpov.tm.entity.User;
import ru.karpov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IUserEndpoint {

    @NotNull
    @WebMethod
    User findOneUser(@NotNull @WebParam(name = "session") final Session session,
                     @NotNull @WebParam(name = "login") final String login
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Collection<@NotNull User> findAllUsers(@NotNull @WebParam(name = "session") final Session session) throws AbstractException;

    @WebMethod
    void persistUser(@NotNull @WebParam(name = "user") final User user) throws AbstractException;

    @WebMethod
    void mergeUser(@NotNull @WebParam(name = "session") final Session session,
                   @NotNull @WebParam(name = "user") final User user
    ) throws AbstractException;

    @WebMethod
    void removeUser(@NotNull @WebParam(name = "session") final Session session,
                    @NotNull @WebParam(name = "login") final String login
    ) throws AbstractException;

    @WebMethod
    boolean containsUser(@NotNull @WebParam(name = "login") final String login) throws AbstractException;

//    @NotNull
//    @WebMethod
//    Map<String, User> getAllUserEntities(@NotNull @WebParam(name = "session") final Session session) throws AbstractException;
}
