package ru.karpov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.entity.Task;

import java.util.Collection;
import java.util.Map;

public interface ITaskRepository {
    @NotNull Task findOne(@NotNull final String taskId);

    @NotNull Collection<@NotNull Task> findAll(@NotNull final String userId);

    void persist(@NotNull final Task task);

    void merge(@NotNull final Task task);

    void remove(@NotNull final String taskId);

    void removeAll(@NotNull final String userId);

    boolean contains(@NotNull final String taskId);

    @NotNull Map<String, Task> getAllEntities();
}
