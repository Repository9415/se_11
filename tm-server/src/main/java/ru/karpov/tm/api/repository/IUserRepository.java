package ru.karpov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.entity.User;

import java.util.Collection;

public interface IUserRepository {
    @NotNull User findOne(@NotNull final String login);

    @NotNull Collection<@NotNull User> findAll();

    void persist(@NotNull final User user);

    void merge(@NotNull final User user);

    void remove(@NotNull final String login);

    boolean contains(@NotNull final String login);

//    @NotNull Map<String, User> getAllEntities();
}
