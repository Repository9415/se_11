package ru.karpov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.repository.IProjectRepository;
import ru.karpov.tm.api.repository.IRepository;
import ru.karpov.tm.api.service.IProjectService;
import ru.karpov.tm.entity.Project;
import ru.karpov.tm.exception.AbstractException;
import ru.karpov.tm.exception.NullParameterInMethodException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
        super.abstractRepository = (IRepository<@NotNull Project>) projectRepository;
    }

    @NotNull
    public Collection<Project> findAll(@NotNull final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) {
            throw new NullParameterInMethodException("Parameter \"userId\" is null");
        }
        return projectRepository.findAll(userId);
    }

    //Обновляет объект. Если объект не существует, то встравляется. Если существует, то обновляет поля объекта.
    public void merge(@NotNull final Project project) throws AbstractException {
        if (project == null) {
            throw new NullParameterInMethodException("Object \"project\" is null");
        }
        if (!projectRepository.contains(project.getId())) {
            projectRepository.merge(project);
            return;
        }
        @NotNull final Project mergeProject = project;
        mergeProject.setName(project.getName());
        mergeProject.setDescription(project.getDescription());
        mergeProject.setDateStart(project.getDateStart());
        mergeProject.setDateFinish(project.getDateFinish());
        projectRepository.merge(mergeProject);
    }

    public void removeAll(@NotNull final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) {
            throw new NullParameterInMethodException("Parameter \"userId\" is null");
        }
        projectRepository.removeAll(userId);
    }

    @NotNull
    public Set<String> getAllProjectNames(@NotNull final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) {
            throw new NullParameterInMethodException("Parameter \"userId\" is null");
        }
        @NotNull final Set<String> nameProjects = new HashSet<>();
        for (@NotNull final Project project : projectRepository.findAll(userId)) {
            if (project.getUserId().equals(userId))
                nameProjects.add(project.getName());
        }
        return nameProjects;
    }
}
