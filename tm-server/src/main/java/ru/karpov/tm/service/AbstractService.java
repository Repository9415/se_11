package ru.karpov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.repository.IRepository;
import ru.karpov.tm.api.service.IService;
import ru.karpov.tm.entity.AbstractEntity;
import ru.karpov.tm.exception.AbstractException;
import ru.karpov.tm.exception.DublicateObjectException;
import ru.karpov.tm.exception.NullParameterInMethodException;

@NoArgsConstructor
public abstract class AbstractService<@NotNull T extends AbstractEntity> implements IService<@NotNull T> {
    @NotNull
    protected IRepository<@NotNull T> abstractRepository;

    @NotNull
    @Override
    public T findOne(@NotNull final String id) throws AbstractException {
        if (id == null || id.isEmpty()) {
            throw new NullParameterInMethodException("Parameter \"id\" is null.");
        }
        return abstractRepository.findOne(id);
    }

    //Вставляет объект. Если объект существует, то выбрасывается исключение. Если не существует, то добавляется.
    @Override
    public void persist(@NotNull final T entity) throws AbstractException {
        if (entity == null) {
            throw new NullParameterInMethodException("Entity of the \"" + entity.getClass().getSimpleName() + "\" class is null.");
        }
        if (abstractRepository.contains(entity.getId())) {
            throw new DublicateObjectException("Entity of the \"" + entity.getClass().getSimpleName() + "\" class already exist in Repository!");
        }
        abstractRepository.persist(entity);
    }

    @Override
    public void remove(@NotNull final String id) throws AbstractException {
        if (id == null || id.isEmpty())
            throw new NullParameterInMethodException("Parameter \"id\" is null.");
        abstractRepository.remove(id);
    }

    @Override
    public boolean contains(@NotNull final String id) throws AbstractException {
        if (id == null || id.isEmpty()) {
            throw new NullParameterInMethodException("Parameter \"id\" is null.");
        }
        return abstractRepository.contains(id);
    }
}
