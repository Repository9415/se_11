package ru.karpov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.repository.IRepository;
import ru.karpov.tm.api.repository.IUserRepository;
import ru.karpov.tm.api.service.IUserService;
import ru.karpov.tm.entity.User;
import ru.karpov.tm.exception.AbstractException;
import ru.karpov.tm.exception.NullParameterInMethodException;

import java.util.Collection;

public final class UserService extends AbstractService<@NotNull User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        this.userRepository = userRepository;
        super.abstractRepository = (IRepository<@NotNull User>) userRepository;
    }

    @NotNull
    public Collection<@NotNull User> findAll() {
        return userRepository.findAll();
    }

    //Обновляет объект. Если объект не существует, то встравляется. Если существует, то обновляет поля объекта.
    public void merge(@NotNull final User user) throws AbstractException {
        if (user == null) {
            throw new NullParameterInMethodException("Parameter \"user\" is null");
        }
        if (!userRepository.contains(user.getId())) {
            userRepository.merge(user);
            return;
        }
        @NotNull final User mergeUser = user;
        mergeUser.setLogin(user.getLogin());
        mergeUser.setPassword(user.getPassword());
        mergeUser.setRole(user.getRole());
        userRepository.merge(mergeUser);
    }
}
