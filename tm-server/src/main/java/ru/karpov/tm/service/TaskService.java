package ru.karpov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.repository.IRepository;
import ru.karpov.tm.api.repository.ITaskRepository;
import ru.karpov.tm.api.service.ITaskService;
import ru.karpov.tm.entity.Task;
import ru.karpov.tm.exception.AbstractException;
import ru.karpov.tm.exception.NullParameterInMethodException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public final class TaskService extends AbstractService<@NotNull Task> implements ITaskService {

    @NotNull
    private ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskPepository) {
        this.taskRepository = taskPepository;
        super.abstractRepository = (IRepository<Task>) taskRepository;
    }

    @NotNull
    public Collection<@NotNull Task> findAll(@NotNull final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) {
            throw new NullParameterInMethodException("Parameter \"userId\" is null");
        }
        return taskRepository.findAll(userId);
    }

    //Обновляет объект. Если объект не существует, то встравляется. Если существует, то обновляет поля объекта.
    public void merge(@NotNull final Task task) throws AbstractException {
        if (task == null) {
            throw new NullParameterInMethodException("Parameter \"task\" is null");
        }
        if (!taskRepository.contains(task.getId())) {
            taskRepository.merge(task);
            return;
        }
        @NotNull final Task mergeTask = task;
        mergeTask.setName(task.getName());
        mergeTask.setDescription(task.getDescription());
        mergeTask.setDateStart(task.getDateStart());
        mergeTask.setDateFinish(task.getDateFinish());
        taskRepository.merge(mergeTask);
    }

    public void removeAll(@NotNull final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) {
            throw new NullParameterInMethodException("Parameter \"userId\" is null");
        }
        taskRepository.removeAll(userId);
    }

    @NotNull
    public Set<String> getAllTaskNames(@NotNull final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) {
            throw new NullParameterInMethodException("Parameter \"userId\" is null");
        }
        @NotNull final Set<String> nameTasks = new HashSet<>();
        for (@NotNull final Task task : taskRepository.findAll(userId)) {
            if (task.getUserId().equals(userId))
                nameTasks.add(task.getName());
        }
        return nameTasks;
    }
}
