package ru.karpov.tm.service;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karpov.tm.api.repository.IRepository;
import ru.karpov.tm.api.repository.ISessionRepository;
import ru.karpov.tm.api.service.ISessionService;
import ru.karpov.tm.constant.Constant;
import ru.karpov.tm.entity.Session;
import ru.karpov.tm.enumerated.Role;
import ru.karpov.tm.exception.AbstractException;
import ru.karpov.tm.exception.NullParameterInMethodException;
import ru.karpov.tm.exception.SessionNotValidException;

import java.util.Collection;

@Setter
@NoArgsConstructor
public class SessionService extends AbstractService<@NotNull Session> implements ISessionService {

    @NotNull
    private ISessionRepository sessionRepository;

    public SessionService(@NotNull final ISessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
        super.abstractRepository = (IRepository<Session>) sessionRepository;
    }

    @Override
    public void validateSessionForAdmin(@NotNull final Session session) throws AbstractException {
        if (session == null || session.getRole() != Role.ADMIN) throw new SessionNotValidException("Admin role required");
        validateSession(session);
    }

    @Override
    public void validateSession(@NotNull final Session session) throws AbstractException {
        if (session == null || session.getUserId() == null || session.getUserId().isEmpty() || session.getRole() == null)
            throw new SessionNotValidException();
        @NotNull final long durationSessionTime = System.currentTimeMillis() - session.getTimeStamp();
        if (durationSessionTime > Constant.SESSION_LIFE_TIME) throw new SessionNotValidException();
        @NotNull final Session sessionOfServer = findOne(session.getUserId(), session.getId());
        if (sessionOfServer == null || sessionOfServer.getSignature() == null)
            throw new SessionNotValidException();
        if (!sessionOfServer.getSignature().equals(session.getSignature()))
            throw new SessionNotValidException();
    }

    @NotNull
    @Override
    public Session findOne(@Nullable final String userId, @Nullable final String sessionId) throws AbstractException {
        final boolean isNullUserId = userId == null || userId.isEmpty();
        final boolean isNullId = sessionId == null || sessionId.isEmpty();
        if (isNullUserId || isNullId) {
            throw new NullParameterInMethodException("Session parameter is null");
        }
        return sessionRepository.findOne(sessionId);
    }

    @Override
    public void merge(@NotNull final Session session) throws AbstractException {
        if (session == null) {
            throw new NullParameterInMethodException("Parameter \"session\" is null");
        }
        if (!sessionRepository.contains(session.getId())) {
            sessionRepository.merge(session);
            return;
        }
        @NotNull final Session mergeSession = session;
        mergeSession.setUserId(session.getUserId());
        mergeSession.setRole(session.getRole());
        mergeSession.setSignature(session.getSignature());
        sessionRepository.merge(mergeSession);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String sessionId) throws AbstractException {
        final boolean isNullUserId = userId == null || userId.isEmpty();
        final boolean isNullId = sessionId == null || sessionId.isEmpty();
        if (isNullUserId || isNullId) {
            throw new NullParameterInMethodException("Session parameter is null");
        }
        sessionRepository.remove(userId, sessionId);
    }

    @NotNull
    @Override
    public Collection<@NotNull Session> findAll() throws AbstractException {
        return sessionRepository.findAll();
    }
}
