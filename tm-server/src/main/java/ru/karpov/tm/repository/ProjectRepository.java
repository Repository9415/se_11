package ru.karpov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.repository.IProjectRepository;
import ru.karpov.tm.entity.Project;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<@NotNull Project> implements IProjectRepository {

    @NotNull
    @Override
    public Collection<@NotNull Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> userProjects = new LinkedList<>();
        for (@NotNull final Project project : entityMap.values()) {
            if (project.getUserId().equals(userId)) {
                userProjects.add(project);
            }
        }
        return userProjects;
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final Iterator<Project> iterProject = entityMap.values().iterator();
        while (iterProject.hasNext()) {
            @NotNull final Project project = iterProject.next();
            if (project.getUserId().equals(userId)) {
                iterProject.remove();
            }
        }
    }
}
