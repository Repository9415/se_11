package ru.karpov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.repository.ITaskRepository;
import ru.karpov.tm.entity.Task;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<@NotNull Task> implements ITaskRepository {

    @NotNull
    @Override
    public Collection<@NotNull Task> findAll(@NotNull final String userId) {
        @NotNull final List<Task> userTasks = new LinkedList<>();
        for (@NotNull final Task task : entityMap.values()) {
            if (task.getUserId().equals(userId)) {
                userTasks.add(task);
            }
        }
        return userTasks;
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        entityMap.values().removeIf(task -> task.getUserId().equals(userId));
    }
}
