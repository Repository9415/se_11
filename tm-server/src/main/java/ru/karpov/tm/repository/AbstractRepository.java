package ru.karpov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.repository.IRepository;
import ru.karpov.tm.entity.AbstractEntity;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractRepository<@NotNull T extends AbstractEntity> implements IRepository<@NotNull T> {

    @NotNull
    protected final Map<String, T> entityMap = new LinkedHashMap<>();

    @NotNull
    @Override
    public T findOne(@NotNull final String keyMap) {
        return entityMap.get(keyMap);
    }

    @Override
    public void persist(@NotNull final T entity) {
        entityMap.put(entity.getId(), entity);
    }

    @Override
    public void merge(@NotNull final T entity) {
        entityMap.put(entity.getId(), entity);
    }

    @Override
    public void remove(@NotNull final String entity) {
        entityMap.remove(entity);
    }

    @Override
    public boolean contains(@NotNull final String keyMap) {
        return entityMap.containsKey(keyMap);
    }

    @NotNull
    @Override
    public Map<String, T> getAllEntities() {
        return entityMap;
    }
}
