package ru.karpov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.repository.ISessionRepository;
import ru.karpov.tm.entity.Session;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public final class SessionRepository extends AbstractRepository<@NotNull Session> implements ISessionRepository {

    @NotNull
    @Override
    public Session findOne(@NotNull final String userId, @NotNull final String sessionId) {
        @NotNull final List<Session> userSessions = new LinkedList<>();
        for (@NotNull final Session session : entityMap.values()) {
            if (session.getUserId().equals(userId)) {
                userSessions.add(session);
            }
        }
        for (@NotNull final Session session : userSessions) {
            if (session.getUserId().equals(sessionId)) {
                return session;
            }
        }
        return new Session();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) {
        entityMap.values().removeIf(session -> session.getUserId().equals(userId));
    }

    @NotNull
    @Override
    public Collection<@NotNull Session> findAll() {
        return entityMap.values();
    }
}
