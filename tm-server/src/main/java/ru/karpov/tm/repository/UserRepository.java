package ru.karpov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.karpov.tm.api.repository.IUserRepository;
import ru.karpov.tm.entity.User;

import java.util.Collection;

public final class UserRepository extends AbstractRepository<@NotNull User> implements IUserRepository {

    @NotNull
    @Override
    public Collection<@NotNull User> findAll() {
        return entityMap.values();
    }

    @Override
    public void persist(@NotNull final User user) {
        entityMap.put(user.getLogin(), user);
    }

    @Override
    public void merge(@NotNull final User user) {
        persist(user);
    }
}
